#!/bin/bash
release_build=""

if [[ $PATH != *"dotnet"* ]]; then
    echo "You do not have dotnet SDK installed, would you like to automatically install it? (y/n): "
    while [ true ]
    do
        read answer

        if [ "$answer" == 'y' ] || [ "$answer" == 'n' ]; then
            break
        else
            echo "I didn't quite get that. Would you like to automatically install it? (y/n): "
        fi

    done

    if [ "$answer" == "y" ]; then
        if [ $UID != 0 ]; then
            sudo "./build.sh"
        fi

        tmp_file=$(mktemp)+".sh"
        curl -sSL https://dot.net/v1/dotnet-install.sh -o $tmp_file
        bash $tmp_file
        wait
    else
        echo "Ok, please install the .NET Core SDK to build."
        exit -1
    fi
fi

case $(uname -m) in
    i386)
        echo "Too old, try more modern hardware."
        exit -1;;
    i686) release_build="linux-x86";;
    x86_64) release_build="linux-x64";;
    arm) release_build="linux-arm";;
esac

dotnet publish -r $release_build -c Release --self-contained true "Fluffybot 2.sln" -o "./Published Build/"

echo "Would you like to install the bot now? (y/n): "
read answer

if [ "$answer" != "y" ]; then
    exit 0
fi

if [ $UID != 0 ]; then
    sudo "./build.sh"
fi

if ![ -d "/opt/Fluffybot" ]; then
    mkdir "/opt/Fluffybot"
fi

echo "Moving compiled files to install directory."

mv -vr "./Published Build/*" "/opt/Fluffybot"

echo "Linking binary to bin directory."

ln -sf "/usr/bin/fluffybot" "/opt/Fluffybot/Fluffybot"

if ![ -f "/etc/systemd/system/fluffybot.service" ]; then
    echo "Creating service file."

    echo "[Unit]
Description=FluffyBot Discord Bot Service
# Ensures that the bot has access to the network.
After=network.target

[Service]
# Starts the bot executable.
ExecStart=/usr/bin/fluffybot

[Install]
# The thing to start the service with.
WantedBy=multi-user.target" >> /etc/systemd/system/fluffybot.service
else
    systemctl stop fluffybot.service
    exists=true
fi

/bin/id 666 2>/dev/null

if [ $? -eq 1 ]; then
    echo "Adding FluffyBot user."

    useradd -r -u 666 -g 666 -s /bin/false fluffybot
fi

echo "Making FluffyBot own it's things, along with restricting write permissions."

chown -vR 666:666 /opt/FluffyBot
chmod -vR 755 /opt/FluffyBot

if [ $exists == false ]; then
    echo "Enabling the bot in SystemD."

    systemctl enable fluffybot.service

    echo "Please paste the Discord token now: "
    read token

    if ![ -d "/etc/fluffybot" ]; then
        mkdir "/etc/fluffybot"
    fi

    echo "{\"token\":\"$token\"}" > "/etc/fluffybot/global.json"
    
    chown -vR 666:666 "/etc/fluffybot"
fi

echo "Starting bot up."

systemctl start fluffybot.service