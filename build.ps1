$architecture = ""
$installPath = "C:/Program Files/FluffyBot"

if ( !( $env:Path -like '*dotnet.exe*' ) ) {
    Write-Output "Dotnet is not installed on your machine, please install dotnet SDK via the site dot.net."

    exit -1
}

switch($env:PROCESSOR_ARCHITECTURE) {
    "IA64" {}
    "AMD64" {
        $architecture = "win-x64"
        break; }
    "x86" {
        $architecture = "win-x86"
        break;
    }
}

dotnet publish -r $architecture -c Release --self-contained true -o "./Published Build" "Fluffybot 2.sln"

if ( -not TestPath $installPath -PathType any ) {
    try {
        New-Time -Path $installPath -ItemType Directory -ErrorAction Stop | Out-Null
	} catch {
        Write-Error -Message "Unable to create directory at `$installPath`. Error was: $_" -ErrorAction Stop
	}

    Write-Output "Successfully created install directory."
}

Move-Item -Path "./Published Build" -Recurse -Destination $installPath