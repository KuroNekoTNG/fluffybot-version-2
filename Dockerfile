#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

ARG RTIME=5.0
FROM mcr.microsoft.com/dotnet/runtime:$RTIME-buster-slim AS base
WORKDIR /app

ARG RTIME=5.0
FROM mcr.microsoft.com/dotnet/sdk:$RTIME AS build
ARG BCONFIG=Release
ARG FWORK=net5.0
RUN echo $RTIME $FWORK $BCONFIG
WORKDIR /src
COPY ["Fluffybot 2/FluffyBot.csproj", "Fluffybot 2/"]
COPY ["dep/eSixSharp/e6Sharp/e6Sharp.csproj", "dep/eSixSharp/e6Sharp/"]
COPY ["Tar/Tar.csproj", "Tar/"]
RUN dotnet restore -v d "Fluffybot 2/FluffyBot.csproj"
COPY . .
WORKDIR "/src/Fluffybot 2"
RUN dotnet build "FluffyBot.csproj" -c $BCONFIG -f $FWORK -o /app/build

FROM build AS publish
ARG BCONFIG=Release
ARG FWORK=net5.0
RUN dotnet publish "FluffyBot.csproj" -c $BCONFIG -f $FWORK -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .

ENTRYPOINT ["./FluffyBot", "--print-to-console"]