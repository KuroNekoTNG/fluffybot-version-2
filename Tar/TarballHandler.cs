﻿using System;
using System.IO;
using System.IO.Compression;
using System.Text;

/// <summary>
/// The namespace to handle tarballs.
/// </summary>
namespace Tar {

	public abstract class BaseData {

		public virtual bool IsDirectory {
			get => new FileInfo( FileName ).Attributes.HasFlag( FileAttributes.Directory );
		}

		public virtual string FileName {
			get;
		}

		protected BaseData( string fileName ) {
			FileName = fileName;
		}

	}

	/// <summary>
	/// Used to handle a direct stream insert into the tarball.
	/// </summary>
	public class StreamData : BaseData {

		public override bool IsDirectory {
			get => false;
		}

		public Stream Data {
			get;
		}

		public StreamData( Stream data, string fileName ) : base( fileName ) {
			Data = data;
		}

		~StreamData() {
			Data.Dispose();
		}

	}

	/// <summary>
	/// Used to handle file insertions into the tarball.
	/// </summary>
	public class FileData : BaseData {

		public FileData( string fileName ) : base( fileName ) { }
	}

	/// <summary>
	/// The type of compression to use on the tarball.
	/// </summary>
	public enum Compression : byte {
		/// <summary>
		/// Leave the tarball uncompressed.
		/// </summary>
		None = 0,
		/// <summary>
		/// Compress the tarball using GZip.
		/// </summary>
		GZip = 1,
		/// <summary>
		/// Compress the tarball using LZMA (7Zip).
		/// </summary>
		/// <remarks>
		/// For this option to work, the SevenZip library must be in the AppDomain.
		/// </remarks>
		LZMA = 2,
	}

	/// <summary>
	/// Handles tarball creation.
	/// </summary>
	/// <remarks>
	/// Handles things with the creation of a tarball.
	/// It does not care if it is on Linux or Windows or any OS out there.
	/// Maybe in the future it will also handle extracting of tarballs, but at the time it does not.
	/// </remarks>
	public static class TarballHandler {

		/// <summary>
		/// Creates an optionally compressed tarball.
		/// </summary>
		/// <remarks>
		/// Creates a region of memory where a tarball, that is optionally compressed, is held at without using a <see cref="MemoryStream"/>.
		/// To grab a stream version, call <see cref="CreateStream(Compression, BaseData[])"/>.
		/// For a known uncompressed, call either <see cref="CreateRaw(BaseData[])"/> or <see cref="CreateStream(BaseData[])"/>.
		/// </remarks>
		/// <param name="compression">The type of compression to use.</param>
		/// <param name="datas">The <see cref="BaseData"/> to insert into the tarball.</param>
		/// <returns>An area of memory holding the optionally compressed tarball.</returns>
		public static byte[] CreateRaw( Compression compression, params BaseData[] datas ) {
			using ( var tarStream = CreateStream( compression, datas ) ) {
				return tarStream.ToArray();
			}
		}

		/// <summary>
		/// Creates a raw array of a tarball.
		/// </summary>
		/// <remarks>
		/// Creates a region of memory where a tarball is held without the use of a <see cref="MemoryStream"/>.
		/// This tarball is uncompressed, to use compression use <see cref="CreateRaw(Compression, BaseData[])"/>.
		/// For the stream version, call either <see cref="CreateStream(BaseData[])"/> or <see cref="CreateStream(Compression, BaseData[])"/>.
		/// </remarks>
		/// <param name="datas">The <see cref="BaseData"/> to insert into the tarball.</param>
		/// <returns>An area of memory holding an uncompressed tarball.</returns>
		public static byte[] CreateRaw( params BaseData[] datas ) {
			using ( var tarStream = CreateStream( datas ) ) {
				return tarStream.ToArray();
			}
		}

		/// <summary>
		/// Creates a readonly compressed <see cref="Stream"/> of a tarball.
		/// </summary>
		/// <remarks>
		/// Creates an optionall compressed tarball <see cref="MemoryStream"/>.
		/// GZip is always avialable since .NET comes with <see cref="GZipStream"/>.
		/// LZMA support is optional and requires the SevenZip library to use it.
		/// For getting only the bytes, call <see cref="CreateRaw(Compression, BaseData[])"/> or <see cref="CreateRaw(BaseData[])"/>.
		/// For a gaurentee non-compressed tarball, call <see cref="CreateStream(BaseData[])"/> or use <see cref="Compression.None"/>.
		/// </remarks>
		/// <param name="compression">The type of compression to use.</param>
		/// <param name="datas">The <see cref="BaseData"/> to insert into the tarball.</param>
		/// <returns>A compressed readonly <see cref="MemoryStream"/> of a tarball.</returns>
		public static MemoryStream CreateStream( Compression compression, params BaseData[] datas ) {
			const string LZMA_TYPE = "SevenZip.Compression.LZMA.Encoder";
			var tar = CreateStream( datas );

			switch ( compression ) {
				case Compression.None:
					return tar;
				case Compression.GZip:
					using ( var memStream = new MemoryStream() ) {
						using ( var gzip = new GZipStream( memStream, CompressionLevel.Optimal, true ) ) {
							gzip.Write( tar.ToArray() );
						}

						tar.Dispose();

						return new MemoryStream( memStream.ToArray(), false );
					}
				case Compression.LZMA:
					var encType = Type.GetType( LZMA_TYPE, false );

					if ( encType is null ) {
						throw new NotSupportedException( "LZMA support is not enabled since the 7Zip lib is missing." );
					}

					var enc = Activator.CreateInstance( encType );
					var coderPropMethod = encType.GetMethod( "WriteCoderProperties" );
					var encCode = encType.GetMethod( "Code" );

					using ( var outStream = new MemoryStream() ) {
						coderPropMethod.Invoke( enc, new[] { outStream } );

						outStream.Write( BitConverter.GetBytes( tar.Length ) );

						encCode.Invoke( enc, new object[] { tar, outStream, tar.Length, -1, null } );

						tar.Dispose();

						return new MemoryStream( outStream.ToArray(), false );
					}
				default:
					tar.Dispose();

					throw new NotSupportedException( $"Unknown type: {Enum.GetName( typeof( Compression ), compression )}" );
			}
		}

		/// <summary>
		/// Creates a readonly <see cref="Stream"/> of the tarball.
		/// </summary>
		/// <remarks>
		/// This method creates an uncompressed tarball.
		/// Because it is uncompressed, this means that the space it takes up is a lot.
		/// For a compressed tarball, call <see cref="CreateStream(Compression, BaseData[])"/>.
		/// While this provides raw, readonly access to the tarball, for a non-stream version, call
		/// <see cref="CreateRaw(BaseData[])"/> or <see cref="CreateRaw(Compression, BaseData[])"/>.
		/// </remarks>
		/// <param name="datas">The <see cref="BaseData"/> that is meant to be commited to the tarball.</param>
		/// <returns>A readonly <see cref="MemoryStream"/> that contains the tarball.</returns>
		public static MemoryStream CreateStream( params BaseData[] datas ) {
			// Data is to be inserted into the block first, so as to keep this simple.
			var block = new byte[512];
			var stream = default( Stream );
			using ( var memStream = new MemoryStream() ) {

				if ( datas is null ) {
					throw new ArgumentNullException( nameof( datas ), "The files/data to insert into the tarball is null." );
				}

				if ( datas.Length == 0 ) {
					throw new ArgumentOutOfRangeException( nameof( datas ), "The array of files/data contains nothing, something must be inserted." );
				}

				foreach ( var data in datas ) {
					var offset = ( ushort )0;

					/* Insert data is called multiple times in order to insert data without having to do anything fancy.
					 * Why not just insert it directly into the memory stream? Well that is easy.
					 * Since tarballs are 512 blocks big, and the byte array is 512 big, modifying the block would be easier.
					 * It also allows for a constant 512 data to be inserted into the tarball every write.
					 * This means that there doesn't have to be any calculations to change data for a bunch data,
					 * nor does there have to be a bunch of for loops cluttering up the code and making it harder to read.
					 * This method will act upon that block with ease and does everything that is needed. */
					InsertData( ref block, ref offset, GenerateFileNameField( data.FileName ) );
					InsertData( ref block, ref offset, new byte[] {
						0x30, 0x30, 0x30, 0x30, 0x37, 0x35, 0x35, 0x0,	// 0000755 - I don't know what these are for.
						0x30, 0x30, 0x30, 0x31, 0x37, 0x35, 0x30, 0x0,	// 0001750
						0x30, 0x30, 0x30, 0x31, 0x37, 0x35, 0x30, 0x0,	// 0001750
					} );

					if ( data is StreamData sd ) {
						InsertData( ref block, ref offset, GenerateAsciiOctal( sd.Data.Length ) );

						stream = sd.Data;
					} else if ( data is FileData fd && !fd.IsDirectory ) {
						stream = new FileStream( fd.FileName, FileMode.Open, FileAccess.Read, FileShare.Read );

						InsertData( ref block, ref offset, GenerateAsciiOctal( stream.Length ) );
					} else {
						InsertData( ref block, ref offset, GenerateAsciiOctal( 0 ) );
					}

					InsertData( ref block, ref offset, new byte[] { 0 } );
					InsertData( ref block, ref offset, GenerateAsciiOctal( GetTimeSinceEpoch() ) );
					InsertData( ref block, ref offset, new byte[] { 0 } );
					InsertData( ref block, ref offset, new byte[] { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 /* Treat as spaces. */ } );
					InsertData( ref block, ref offset, new byte[] { 0x00 } );
					InsertData( ref block, ref offset, GenerateFileNameField( string.Empty ) );

					// Reset the offset back to where the checksum should be.
					offset = 148;

					// Writes out the checksum here.
					InsertData( ref block, ref offset, GenerateChecksum( block ) );
					InsertData( ref block, ref offset, new byte[] { 0x00, 0x20 } );

					// Commits the header to the tar file.
					memStream.Write( block );

					// Reset the buffer to 0.
					Array.Fill( block, ( byte )0 );

					// Checks to see if the stream is empty.
					if ( stream is null ) {
						continue;
					}

					// Writes 512-blocks to the stream.
					while ( 0 < stream.Read( block ) ) {
						memStream.Write( block );

						// Reset the buffer to 0.
						Array.Fill( block, ( byte )0 );
					}
				}

				// 2 empty entries as per the spec.
				memStream.Write( block );
				memStream.Write( block );

				return new MemoryStream( memStream.ToArray(), false );
			}
		}

		private static int GetTimeSinceEpoch() => ( int )DateTime.UtcNow.Subtract( DateTime.UnixEpoch ).TotalSeconds;

		private static byte[] GenerateChecksum( in byte[] block ) {
			var checksum = 0u;

			foreach ( var @byte in block ) {
				checksum += @byte;
			}

			return GenerateAsciiOctal( checksum, 6 );
		}

		private static byte[] GenerateAsciiOctal( long num, byte size = 11 ) {
			var strSize = new char[size];

			for ( var i = 0; i < size; ++i ) {
				var powNum = ( long )Math.Pow( 8, size - i - 1 );

				strSize[i] = ( num / powNum ) switch
				{
					0 => '0',
					1 => '1',
					2 => '2',
					3 => '3',
					4 => '4',
					5 => '5',
					6 => '6',
					7 => '7',
					_ => throw new Exception( "Somethings not right." ),
				};

				num %= powNum;
			}

			return Encoding.ASCII.GetBytes( strSize );
		}

		private static void InsertData( ref byte[] header, ref ushort offset, in byte[] data ) {
			for ( var i = offset; i - offset < data.Length; ++i ) {
				header[i] = data[i - offset];
			}

			offset = ( ushort )( offset + data.Length );
		}

		private static byte[] GenerateFileNameField( string fileName ) {
			var field = new byte[100];
			var strBytes = Encoding.ASCII.GetBytes( fileName );

			for ( var i = 0; i < strBytes.Length; ++i ) {
				field[i] = strBytes[i];
			}

			return field;
		}
	}
}
