﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using FluffyBot.Database;
using DbImage = FluffyBot.Database.Image;
using Image = FluffyBot.Database.Image;

namespace FluffyBot.Modules {
	// ReSharper disable once UnusedType.Global
	[SuppressMessage( "ReSharper", "UnusedMember.Global" )]
	public sealed class BasicCommands : ModuleBase<SocketCommandContext> {
		private readonly FluffyBotContext dbContext;

		public BasicCommands( FluffyBotContext ctx ) {
			dbContext = ctx;
		}

		[Command( "HardwareInfo", true, RunMode = RunMode.Async )]
		public Task MyHardware() {
			var osArch        = RuntimeInformation.OSArchitecture;
			var procArch      = RuntimeInformation.ProcessArchitecture;
			var osDesc        = RuntimeInformation.OSDescription;
			var frameworkDesc = RuntimeInformation.FrameworkDescription;
			var embedBuilder = new EmbedBuilder() {
				Color       = Color.DarkPurple,
				Description = "Information about me.",
				Timestamp   = DateTimeOffset.Now,
				Title       = "Current Hardware/Software Information",
			};

			embedBuilder.AddField( "OS Architecture", osArch );
			embedBuilder.AddField( "OS Desc", osDesc );
			embedBuilder.AddField( "Process Architecture", procArch );
			embedBuilder.AddField( "Framework Desc", frameworkDesc );

			return Context.Channel.SendMessageAsync( embed: embedBuilder.Build() );
		}

		[Command( "PopImage", RunMode = RunMode.Async )]
		public Task PrintImage( string id ) {
			var img = Guid.TryParse( id, out var guid )
				? dbContext.Images.FirstOrDefault( dbImg => dbImg.Id == guid )
				: dbContext.Images.FirstOrDefault( dbImg => string.Equals( dbImg.Name, id, StringComparison.OrdinalIgnoreCase ) );

			if ( img is null ) {
				return Context.Channel.SendMessageAsync( $"Could not find the image associated with `{id}`." );
			}

			var embedBuilder = new EmbedBuilder() {
				Title    = $"***{img.Name}***",
				ImageUrl = img.Url,
			};

			return Context.Channel.SendMessageAsync( embed: embedBuilder.Build() );
		}

		[Command( "PushImage" )]
		public Task SetImage( string name, string url ) {
			if ( name.Length > 16 ) {
				return Context.Channel.SendMessageAsync( "Sorry, but the name is too long. Max is 16 chars long." );
			}

			var img = new Image {
				Id     = new Guid(),
				Name   = name.ToLower(),
				Url    = url,
				UserID = Context.User.Id
			};

			dbContext.Images.Add( img );

			dbContext.SaveChanges();

			return Context.Channel.SendMessageAsync( $"Success! Image can be brought up via `{name}` or `{img.Id}`." );
		}

		[Command( "PeekImage" )]
		public async Task ListImages() {
			var embedBuilder = new EmbedBuilder();
			var images       = dbContext.Images.ToArray();

			using var typingState = Context.Channel.EnterTypingState();

			for ( var i = 0; i < images.Length; ++i ) {
				embedBuilder.AddField( $"Image {i}", images[i].Name, true );

				if ( ( i == 0 || i % 25 != 0 ) && i != images.Length - 1 ) {
					continue;
				}

				embedBuilder.Title = $"{i - embedBuilder.Fields.Count} - {i}";

				await Context.Channel.SendMessageAsync( embed: embedBuilder.Build() );

				embedBuilder.Fields.Clear();
			}
		}

		[Command( "Latency", true, RunMode = RunMode.Sync )]
		public Task PingMe() => Context.Channel.SendMessageAsync( $"Latency: {Context.Client.Latency} ms" );

		[Command( "Ping" )]
		public Task Ping() => Context.User.SendMessageAsync( "Pong" );

		[Command( "Foss" )]
		public Task ShowMeCode() => Context.Channel.SendMessageAsync( "Here is my source code, UwU: https://gitlab.com/KuroNekoTNG/fluffybot-version-2" );
	}
}