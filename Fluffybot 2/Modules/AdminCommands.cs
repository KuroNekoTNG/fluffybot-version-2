﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using FluffyBot.Database;
using FluffyBot.Utilities;
using Tar;

namespace FluffyBot.Modules {
	// ReSharper disable once UnusedType.Global
	[SuppressMessage( "ReSharper", "UnusedMember.Global" )]
	public sealed class AdminCommands : ModuleBase<SocketCommandContext> {
		private static MemoryStream LoadFilesIntoTarball( params string[] files ) {
			var data = new List<BaseData>();

			foreach ( var file in files ) {
				var fStream = new FileStream( file, FileMode.Open, FileAccess.Read, FileShare.Read );

				data.Add( new StreamData( fStream, new FileInfo( file ).Name ) );
			}

			return TarballHandler.CreateStream( Compression.GZip, data.ToArray() );
		}

		[Command( "Restart" )]
		[RequireOwner]
		public Task Restart() {
			var proc = new Process {
				StartInfo = {
					FileName = Process.GetCurrentProcess()!.MainModule!.FileName,
				},
			};

			Context.Client.LogoutAsync().ConfigureAwait( false ).GetAwaiter().GetResult();

			proc.Start();

			Environment.Exit( 1 );

			return Task.CompletedTask;
		}

		[Command( "CleanMessagesUp" )]
		[RequireUserPermission( GuildPermission.ManageMessages | GuildPermission.KickMembers )]
		public async Task CleanMessages( int count ) {
			++count;

			foreach ( var message in await Context.Channel.GetMessagesAsync( count ).FlattenAsync().ConfigureAwait( false ) ) {
				await message.DeleteAsync();
				await Task.Delay( 256 );
			}
		}

		[Command( "GetSettings" )]
		[RequireUserPermission( GuildPermission.Administrator )]
		public async Task GetGuildSettings() {
			var guildSettings = SettingsManager.GetSettingFile( Context.Guild );

			await Context.User.SendFileAsync( guildSettings, $"Here is the guild settings for `{Context.Guild.Name}`." );
			await Context.Message.DeleteAsync();
		}

		[Command( "GetAllSettings" )]
		[RequireOwner]
		public async Task GetAllSettings() {
			var settingFiles = Directory.GetFiles( SettingsManager.SETTING_FOLDER, "*.json", SearchOption.TopDirectoryOnly );

			await Context.Message.DeleteAsync();

			await using var memStream = LoadFilesIntoTarball( settingFiles );

			await Context.User.SendFileAsync( memStream, "Settings.tar.gz", "Here are all of the settings." );
		}

		[Command( "GetDatabaseFile" )]
		[RequireOwner]
		public async Task GetDatabase() {
			await Context.Message.DeleteAsync();

			await using var memStream = LoadFilesIntoTarball( FluffyBotContext.DatabaseFile );

			await Context.User.SendFileAsync( memStream, "FluffyBot.sqlite", "Here is that database file." );
		}

		[Command( "GetDatabaseAndSettings" )]
		[RequireOwner]
		public async Task GetSettingsAndDatabase() {
			var files = new List<string>( Directory.GetFiles( SettingsManager.SETTING_FOLDER, "*.json", SearchOption.TopDirectoryOnly ) ) {
				FluffyBotContext.DatabaseFile
			};

			await Context.Message.DeleteAsync();

			await using var memStream = LoadFilesIntoTarball( files.ToArray() );
			await Context.User.SendFileAsync( memStream, "Everything.tar.gz", "Here is all the settings." );
		}
	}
}