﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Discord;
using Discord.Commands;
using FluffyBot.Utilities;
using Newtonsoft.Json;

namespace FluffyBot.Modules.Furry.Furbooru {
	[SuppressMessage( "ReSharper", "UnusedMember.Global" )]
	// ReSharper disable once UnusedType.Global
	public sealed class Commands : ModuleBase<SocketCommandContext> {
		private const int    MIN_WAIT_TIME_MS = 500;
		private const string CYCLE_KEY        = "furbooru.cycles";
		private const string BLACKLIST_KEY    = "furbooru.blacklist";
		private const string ANY_IMAGE_SEARCH = "*";
		private const string NSFW_RESPONSE    = "{0}, here is some porn:";
		private const string SFW_RESPONSE     = "{0}, here is some artwork:";

		private static readonly object lockObj = new object();

		private static string RebuildString( IEnumerable<string> strings ) {
			var builder = new StringBuilder();

			foreach ( var str in strings ) {
				builder.Append( str ).Append( ' ' );
			}

			return builder.ToString();
		}

		private static Embed TurnImageIntoEmbed( Image img ) {
			var builder = new EmbedBuilder() {
				Color        = Color.Purple,
				Description  = img.Description.Length <= 1023 ? img.Description : img.Description.Substring( 0, 1023 ),
				ImageUrl     = img.ImageUrl.ToString(),
				ThumbnailUrl = img.ThumbLink.ToString(),
				Timestamp    = img.UpdatedTime,
				Url          = img.Source.ToString(),
				Title        = img.FileName
			};

			builder.AddField( "Width", img.Width, true );
			builder.AddField( "Height", img.Height, true );
			builder.AddField( "Uploader", img.Uploader, true );
			builder.AddField( "Score", img.Score, true );
			builder.AddField( "File Type", img.MimeType, true );
			builder.AddField( "Link", $"[furbooru.org]{img.ImageUrl}", true );

			return builder.Build();
		}

		[Command( "Furbooru.Nsfw.Random" )]
		[RequireNsfw]
		public Task GetRandomNotSafeForWork() {
			var img = GetRandomImage( ANY_IMAGE_SEARCH );

			return Context.Channel.SendMessageAsync( string.Format( NSFW_RESPONSE, Context.User.Mention ), embed: TurnImageIntoEmbed( img ) );
		}

		[Command( "Furbooru.Nsfw" )]
		[RequireNsfw]
		public Task GetNotSafeForWorkImages( /* Don't want the parser to ignore things */ params string[] search ) {
			var fullSearch = RebuildString( search );
			var img        = GetRandomImage( fullSearch );

			return Context.Channel.SendMessageAsync( string.Format( NSFW_RESPONSE, Context.User.Mention ), embed: TurnImageIntoEmbed( img ) );
		}

		[Command( "Furbooru.Sfw.Random" )]
		public Task GetRandomSafeForWork() {
			var img = GetRandomImage( ANY_IMAGE_SEARCH, false );

			return Context.Channel.SendMessageAsync( string.Format( SFW_RESPONSE, Context.User.Mention ), embed: TurnImageIntoEmbed( img ) );
		}

		[Command( "Furbooru.Sfw" )]
		public Task GetSafeForWorkImages( params string[] search ) {
			var fullSearch = RebuildString( search );
			var img        = GetRandomImage( fullSearch, false );

			return Context.Channel.SendMessageAsync( string.Format( SFW_RESPONSE, Context.User.Mention ), embed: TurnImageIntoEmbed( img ) );
		}

		[Command( "Furbooru.Blacklist.Add" )]
		public Task SetBlacklistTags( params string[] tags ) {
			Context.Message.DeleteAsync().ConfigureAwait( false ).GetAwaiter().GetResult();

			if ( Context.User.Id != Context.Guild.OwnerId ) {
				return Context.Channel.SendMessageAsync( $"Only {Context.Guild.Owner.Mention} can set blacklist tags." );
			}

			if ( !SettingsManager.TryGetSetting( BLACKLIST_KEY, Context.Guild, out List<string> blacklist ) ) {
				blacklist = new List<string>();

				SettingsManager.AddSetting( BLACKLIST_KEY, blacklist, Context.Guild );
			}

			var tagCount  = 0;
			var alreadyIn = 0;

			foreach ( var tag in tags ) {
				var index = blacklist.BinarySearch( tag );

				if ( index >= 0 ) {
					++alreadyIn;

					continue;
				}

				blacklist.Insert( ~index, tag );

				++tagCount;
			}

			return Context.User.SendMessageAsync( $"Blacklist has been updated.\nTags Inserted:\t{tagCount}\nTag Already In:\t{alreadyIn}\nBlacklist Size:\t{blacklist.Count}\nType `f!Furbooru.Blacklist.Show` for a list of blacklisted tags." );
		}

		[Command( "Furbooru.Blacklist.Remove" )]
		public Task RemoveFromBlacklist( params string[] tags ) {
			Context.Message.DeleteAsync().ConfigureAwait( false ).GetAwaiter().GetResult();

			if ( Context.User.Id != Context.Guild.OwnerId ) {
				return Context.Channel.SendMessageAsync( $"Only {Context.Guild.Owner.Mention} can remove blacklist tags." );
			}

			if ( !SettingsManager.TryGetSetting( BLACKLIST_KEY, Context.Guild, out List<string> blacklist ) ) {
				return Context.User.SendMessageAsync( "No blacklist found." );
			}

			var notFound = 0;
			var removed  = 0;

			foreach ( var tag in tags ) {
				var index = blacklist.BinarySearch( tag );

				if ( index < 0 ) {
					++notFound;

					continue;
				}

				blacklist.RemoveAt( index );

				++removed;
			}

			return Context.User.SendMessageAsync( $"Blacklist has been updated.\nTags Removed:\t{removed}\nTags Not Found:\t{notFound}\nBlacklist Size:\t{blacklist.Count}\nType `f!Furbooru.Blacklist.Show` for a list of blacklisted tags." );
		}

		[Command( "Furbooru.Blacklist.Show" )]
		public Task ShowBlacklist() {
			Context.Message.DeleteAsync().ConfigureAwait( false ).GetAwaiter().GetResult();

			if ( !SettingsManager.TryGetSetting( BLACKLIST_KEY, Context.Guild, out List<string> blacklist ) ) {
				return Context.User.SendMessageAsync( "There is no blacklist, anything goes." );
			}

			using var memStream = new MemoryStream();
			using var writer    = new StreamWriter( memStream, Encoding.Unicode, leaveOpen: true );

			foreach ( var tag in blacklist ) {
				writer.WriteLine( tag );
			}

			return Context.User.SendFileAsync( memStream, "Blacklist.txt", "Here is the blacklist that you requested." );
		}

		[Command( "Furbooru.MaxCycles.Set" )]
		public Task SetMaxCycles( int maxCycles ) {
			Context.Message.DeleteAsync().ConfigureAwait( false ).GetAwaiter().GetResult();

			if ( Context.User.Id != Context.Guild.OwnerId ) {
				return Context.Channel.SendMessageAsync( $"Only {Context.Guild.Owner.Mention} can change number of pulls." );
			}

			if ( maxCycles != -1 && maxCycles <= 0 ) {
				return Context.User.SendMessageAsync( $"Cycle failed to set:\tValue is invalid, must either be `-1` or `(0, {int.MaxValue}]`." );
			}

			if ( !SettingsManager.HasSetting( CYCLE_KEY, Context.Guild ) ) {
				SettingsManager.AddSetting( CYCLE_KEY, maxCycles, Context.Guild );
			} else {
				SettingsManager.SetSetting( CYCLE_KEY, maxCycles, Context.Guild );
			}

			return Context.User.SendMessageAsync( $"Max cycles has been set to `{maxCycles}`." );
		}

		[Command( "Furbooru.MaxCycles.Show" )]
		public Task ShowMaxCycles() {
			Context.Message.DeleteAsync().ConfigureAwait( false ).GetAwaiter().GetResult();

			if ( !SettingsManager.TryGetSetting( CYCLE_KEY, Context.Guild, out int limit ) ) {
				limit = -1;
			}

			return Context.User.SendMessageAsync( $"# of Pulls:\t{limit}" );
		}

		private Image[] GetImages( string search, Func<Image, bool> decider ) {
			var curCycles = 0;
			var wait      = MIN_WAIT_TIME_MS;
			var images    = new List<Image>();
			var go        = true;

			if ( !SettingsManager.TryGetSetting( CYCLE_KEY, Context.Guild, out int cycles ) ) {
				cycles = -1;
			}

			lock ( lockObj ) {
				do {
					var request = WebRequest.Create( $"https://furbooru.org/api/v1/json/search/images?q={HttpUtility.UrlEncode( search )}&per_page=500&page={++curCycles}&filter_id=34" )
						as HttpWebRequest;
					request!.Headers.Add( HttpRequestHeader.UserAgent, "FluffyBot/Kyu Vulpes" );
					request.AllowAutoRedirect     = true;
					request.UseDefaultCredentials = true;

					var response = (HttpWebResponse) request.GetResponse();

					// No need to implement all as there is a default statement.
					// ReSharper disable once SwitchStatementHandlesSomeKnownEnumValuesWithDefault
					switch ( response.StatusCode ) {
						case HttpStatusCode.OK:
							Image[] jsonImages;
							var     json = GetJsonFromResponse( response.GetResponseStream() );

							if ( json == "[]" ) {
								go = false;

								break;
							}

							try {
								jsonImages = JsonConvert.DeserializeObject<Image[]>( json );
							} catch ( JsonSerializationException ) {
								var jsonDump = Path.Combine( LogManager.MAIN_LOG_FOLDER, "furbooru_response_dump.json" );

								using var jsonDumpStream = new FileStream( jsonDump, FileMode.Create, FileAccess.Write, FileShare.Read );
								using var writer         = new StreamWriter( jsonDumpStream, Encoding.Unicode );

								writer.Write( json );
								writer.Flush();

								throw;
							}

							images.AddRange( jsonImages.Where( decider.Invoke ) );

							if ( curCycles >= cycles ) {
								go = false;
							}

							break;
						case HttpStatusCode.TooManyRequests:
							LogManager.LogWarning( $"Received `{response.StatusDescription}`, waiting {wait} ms." );

							Task.Delay( wait ).ConfigureAwait( false ).GetAwaiter().GetResult();

							wait += 500;

							continue;
						default:
							throw new WebException(
								$"Failed to grab information, got HTTP Status of {(int) response.StatusCode}: {Enum.GetName( typeof( HttpStatusCode ), response.StatusCode )}.",
								null, WebExceptionStatus.UnknownError, response
							);
					}
				} while ( go );
			}

			return images.ToArray();

			static string GetJsonFromResponse( Stream webStream ) {
				string json;

				using ( var reader = new StreamReader( webStream ) ) {
					json = reader.ReadToEnd();
				}

				var firstBracket = json.IndexOf( '[' );
				var lastBracket  = json.IndexOf( "],\"int", StringComparison.Ordinal );
				var length       = lastBracket - firstBracket + 1;

				json = json.Substring( firstBracket, length );

				return json;
			}
		}

		private Image GetRandomImage( string search, bool nsfw = true ) {
			var images = GetImages( search, nsfw ? (Func<Image, bool>) NotSafeForWorkDecider : SafeForWorkDecider );

			return images[new Random().Next( 0, images.Length )];

			bool SafeForWorkDecider( Image img ) {
				if ( img.GuessedRating != Image.Rating.Safe ) {
					return false;
				}

				if ( !SettingsManager.TryGetSetting( BLACKLIST_KEY, Context.Guild, out string[] blacklist ) ) {
					return true;
				}

				foreach ( var blacklistTag in blacklist ) {
					if ( img.Tags.Contains( blacklistTag ) ) {
						return false;
					}
				}

				return true;
			}

			bool NotSafeForWorkDecider( Image img ) {
				if ( img.GuessedRating == Image.Rating.Safe ) {
					return false;
				}

				if ( !SettingsManager.TryGetSetting( BLACKLIST_KEY, Context.Guild, out string[] blacklist ) ) {
					return true;
				}

				foreach ( var blacklistTag in blacklist ) {
					if ( img.Tags.Contains( blacklistTag ) ) {
						return false;
					}
				}

				return true;
			}
		}
	}
}