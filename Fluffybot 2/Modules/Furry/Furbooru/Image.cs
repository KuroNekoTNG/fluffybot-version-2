﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using Newtonsoft.Json;

namespace FluffyBot.Modules.Furry.Furbooru {
	/// <summary>
	/// Represents images from Furbooru.
	/// </summary>
	[SuppressMessage( "ReSharper", "MemberCanBePrivate.Global" )]
	[SuppressMessage( "ReSharper", "UnusedAutoPropertyAccessor.Global" )]
	public sealed class Image {
		/// <summary>
		/// The rating of the image/post from Furbooru.
		/// </summary>
		public enum Rating : sbyte {
			Unknown = -1,
			Safe    = 0,
			Suggestive,
			Questionable,
			Explicit
		}

		/// <summary>
		/// An empty SHA string in case no SHA came through.
		/// </summary>
		private const string EMPTY_SHA512 = "0000000000000000000000000000000000000000000000000000000000000000";

		/// <summary>
		/// If the image is hidden from users.
		/// </summary>
		public bool HiddenFromUsers {
			get;
		}

		/// <summary>
		/// If the image has finished optimization.
		/// </summary>
		public bool Processed {
			get;
		}

		/// <summary>
		/// If this image is hit by the current filter.
		/// </summary>
		public bool Spoilered {
			get;
		}

		/// <summary>
		/// If the thumbnails have been generated or not.
		/// </summary>
		public bool ThumbnailsGenerated {
			get;
		}

		/// <summary>
		/// The number of comments on the image.
		/// </summary>
		public int CommentCount {
			get;
		}

		/// <summary>
		/// The number of down votes the image got.
		/// </summary>
		public int DownVotes {
			get;
		}

		/// <summary>
		/// The number of up votes.
		/// </summary>
		public int UpVotes {
			get;
		}

		/// <summary>
		/// The ID of the target image if it has one.
		/// </summary>
		public int? DuplicationOf {
			get;
		}

		/// <summary>
		/// The number of users who favorite the post.
		/// </summary>
		public int Favorites {
			get;
		}

		/// <summary>
		/// The height of the image in pixels.
		/// </summary>
		public int Height {
			get;
		}

		/// <summary>
		/// The width of the image.
		/// </summary>
		public int Width {
			get;
		}

		/// <summary>
		/// The ID of the image.
		/// </summary>
		public int ID {
			get;
		}

		/// <summary>
		/// THe score of the image.
		/// </summary>
		public int Score {
			get;
		}

		/// <summary>
		/// The user ID that uploaded the post.
		/// </summary>
		public int? UploaderID {
			get;
		}

		/// <summary>
		/// The aspect ratio of the image.
		/// </summary>
		public float AspectRatio {
			get;
		}

		/// <summary>
		/// The Wilson Score of the image.
		/// </summary>
		public float WilsonScore {
			get;
		}

		/// <summary>
		/// The reason for the deletion if there is one.
		/// </summary>
		public string DeletionReason {
			get;
		}

		/// <summary>
		/// The description of the image.
		/// </summary>
		public string Description {
			get;
		}

		/// <summary>
		/// The format of the image.
		/// </summary>
		/// <remarks>
		/// Valid formats from the site are:
		/// <list type="bullet">
		///		<item>GIF</item>
		///		<item>JPEG/JPG</item>
		///		<item>PNG</item>
		///		<item>SVG</item>
		///		<item>WebM</item>
		/// </list>
		/// </remarks>
		public string Format {
			get;
		}

		/// <summary>
		/// The MIME type fo the image.
		/// </summary>
		/// <remarks>
		/// Valid MIME types are:
		/// <list type="bullet">
		///		<item>image/gif</item>
		///		<item>image/jpeg</item>
		///		<item>image/png</item>
		///		<item>image/svg+xml</item>
		///		<item>video/webm</item>
		/// </list>
		/// </remarks>
		public string MimeType {
			get;
		}

		/// <summary>
		/// The name of the file.
		/// </summary>
		public string FileName {
			get;
		}

		/// <summary>
		/// The name of the uploader.
		/// </summary>
		public string Uploader {
			get;
		}

		/// <summary>
		/// The ID of all the tags.
		/// </summary>
		public string[] TagIDs {
			get;
		}

		/// <summary>
		/// All the tags related to the image.
		/// </summary>
		public string[] Tags {
			get;
		}

		/// <summary>
		/// The SHA512 of the original uploaded image.
		/// </summary>
		public byte[] OriginalHash {
			get;
		}

		/// <summary>
		/// The SHA512 of the image after being processed.
		/// </summary>
		public byte[] ImageHash {
			get;
		}

		/// <summary>
		/// The rating of the image.
		/// </summary>
		public Rating GuessedRating {
			get;
		}

		/// <summary>
		/// The date and time of the image creation.
		/// </summary>
		public DateTimeOffset CreatedAt {
			get;
		}

		/// <summary>
		/// The date and time the image was first seen.
		/// </summary>
		public DateTimeOffset FirstSeenAt {
			get;
		}

		/// <summary>
		/// The time the image was updated.
		/// </summary>
		public DateTimeOffset UpdatedTime {
			get;
		}

		/// <summary>
		/// Optional dictionary for the intensity data for deduplication reasons.
		/// </summary>
		public ReadOnlyDictionary<string, float> Intensities {
			get;
		}

		/// <summary>
		/// A dictionary containing the mappings.
		/// </summary>
		public ReadOnlyDictionary<string, Uri> Represenstations {
			get;
		}

		/// <summary>
		/// The URL that leads to the full version of the post.
		/// </summary>
		public Uri FullLink => GetRepresentation( "full" );

		/// <summary>
		/// The URL that leads to the large version of the post.
		/// </summary>
		public Uri LargeLink => GetRepresentation( "large" );

		/// <summary>
		/// The URL that leads to the medium version of the post.
		/// </summary>
		public Uri MediumLink => GetRepresentation( "medium" );

		/// <summary>
		/// The URL that leads to the small version of the post.
		/// </summary>
		public Uri SmallLink => GetRepresentation( "small" );

		/// <summary>
		/// The URL that leads to the tall version of the post.
		/// </summary>
		public Uri TallLink => GetRepresentation( "tall" );

		/// <summary>
		/// The URL that leads to the thumbnail of the post.
		/// </summary>
		public Uri ThumbLink => GetRepresentation( "thumb" );

		/// <summary>
		/// The URL that leads to the small thumbnail.
		/// </summary>
		public Uri ThumbSmallLink => GetRepresentation( "thumb_small" );

		/// <summary>
		/// The URL that leads to the tiny thumbnail.
		/// </summary>
		public Uri ThumbTinyLink => GetRepresentation( "thumb_tiny" );

		/// <summary>
		/// Leads to the post webpage.
		/// </summary>
		public Uri Source {
			get;
		}

		/// <summary>
		/// Leads directly to the actual file.
		/// </summary>
		public Uri ImageUrl {
			get;
		}

		[JsonConstructor]
		internal Image(
			bool hidden_from_users,
			bool processed,
			bool spoilered,
			bool thumbnails_generated,
			int comment_count,
			int downVotes,
			int upVotes,
			int? duplicate_of,
			int faves,
			int height,
			int width,
			int id,
			int score,
			int? uploader_id,
			int tag_count, // This will go unused, but because of Newtonsoft.Json, this has to be here.
			float aspect_ratio,
			float wilson_score,
			string deletion_reason,
			string description,
			string format,
			string mime_type,
			string name,
			string uploader,
			string orig_sha512_hash, // This will get converted to byte[].
			string sha512_hash,      // This will get converted to byte[].
			string source_url,
			string view_url,
			string[] tag_ids,
			string[] tags,
			DateTimeOffset created_at,
			DateTimeOffset first_seen_at,
			DateTimeOffset updated_at,
			Dictionary<string, float> intensities,
			Dictionary<string, string> representations
		) : this(
			hidden_from_users,
			processed,
			spoilered,
			thumbnails_generated,
			comment_count,
			downVotes,
			upVotes,
			duplicate_of,
			faves,
			height,
			width,
			id,
			score,
			uploader_id,
			aspect_ratio,
			wilson_score,
			deletion_reason,
			description,
			format,
			mime_type,
			name,
			uploader,
			orig_sha512_hash,
			sha512_hash,
			tag_ids,
			tags,
			created_at,
			first_seen_at,
			updated_at,
			intensities,
			representations,
			source_url is null ? null : new Uri( source_url ),
			new Uri( view_url )
		) {}


		internal Image(
			bool hidden_from_users,
			bool processed,
			bool spoilered,
			bool thumbnails_generated,
			int comment_count,
			int downVotes,
			int upVotes,
			int? duplicate_of,
			int faves,
			int height,
			int width,
			int id,
			int score,
			int? uploader_id,
			float aspect_ratio,
			float wilson_score,
			string deletion_reason,
			string description,
			string format,
			string mime_type,
			string name,
			string uploader,
			string orig_sha512_hash, // This will get converted to byte[].
			string sha512_hash,      // This will get converted to byte[].
			string[] tag_ids,
			string[] tags,
			DateTimeOffset created_at,
			DateTimeOffset first_seen_at,
			DateTimeOffset updated_at,
			Dictionary<string, float> intensities,
			Dictionary<string, string> representations,
			Uri source_url,
			Uri view_url
		) {
			HiddenFromUsers     = hidden_from_users;
			Processed           = processed;
			Spoilered           = spoilered;
			ThumbnailsGenerated = thumbnails_generated;
			CommentCount        = comment_count;
			DownVotes           = downVotes;
			UpVotes             = upVotes;
			DuplicationOf       = duplicate_of;
			Favorites           = faves;
			Height              = height;
			Width               = width;
			ID                  = id;
			Score               = score;
			UploaderID          = uploader_id;
			AspectRatio         = aspect_ratio;
			WilsonScore         = wilson_score;
			DeletionReason      = deletion_reason;
			Description         = description;
			Format              = format;
			MimeType            = mime_type;
			FileName            = name;
			Uploader            = uploader;
			TagIDs              = tag_ids;
			Tags                = tags;
			CreatedAt           = created_at;
			FirstSeenAt         = first_seen_at;
			UpdatedTime         = updated_at;
			Intensities         = new ReadOnlyDictionary<string, float>( intensities );
			Source              = source_url;
			ImageUrl            = view_url;

			var reps = new Dictionary<string, Uri>();

			foreach ( var pair in representations ) {
				reps.Add( pair.Key, new Uri( pair.Value ) );
			}

			Represenstations = new ReadOnlyDictionary<string, Uri>( reps );

			if ( tags.Contains( "suggestive" ) ) {
				GuessedRating = Rating.Suggestive;
			} else if ( tags.Contains( "safe" ) ) {
				GuessedRating = Rating.Safe;
			} else if ( tags.Contains( "questionable" ) ) {
				GuessedRating = Rating.Questionable;
			} else if ( tags.Contains( "explicit" ) ) {
				GuessedRating = Rating.Explicit;
			} else {
				GuessedRating = Rating.Unknown;
			}

			// Convert hash string to byte[] here.
			// The reason is that chars use 2 bytes, so shrinking it down a size should help enable less memory usage.
			OriginalHash = StringToByteHash( orig_sha512_hash );
			ImageHash    = StringToByteHash( sha512_hash );

			static byte[] StringToByteHash( string hash ) {
				var byteHash = new byte[64];

				for ( var i = 0; i < 64; ++i ) {
					var strByte = hash.Substring( i * 2, 2 );

					byte @byte;
					try {
						@byte = byte.Parse( strByte, NumberStyles.HexNumber );
					} catch ( Exception e ) {
						throw new Exception( "Failed to parse json from Furbooru, please make sure that the json is as it should be.", e );
					}

					byteHash[i] = @byte;
				}

				return byteHash;
			}
		}

		internal Image(
			bool hidden_from_users,
			bool processed,
			bool spoilered,
			bool thumbnails_generated,
			int comment_count,
			int downVotes,
			int upVotes,
			int? duplicate_of,
			int faves,
			int height,
			int width,
			int id,
			int score,
			int? uploader_id,
			float aspect_ratio,
			float wilson_score,
			byte[] orig_sha512_hash,
			byte[] sha512_hash,
			string deletion_reason,
			string description,
			string format,
			string mime_type,
			string name,
			string uploader,
			string[] tag_ids,
			string[] tags,
			DateTimeOffset created_at,
			DateTimeOffset first_seen_at,
			DateTimeOffset updated_at,
			Dictionary<string, float> intensities,
			Dictionary<string, string> representations,
			Uri source_url,
			Uri view_url
		) : this(
			hidden_from_users,
			processed,
			spoilered,
			thumbnails_generated,
			comment_count,
			downVotes,
			upVotes,
			duplicate_of,
			faves,
			height,
			width,
			id,
			score,
			uploader_id,
			aspect_ratio,
			wilson_score,
			deletion_reason,
			description,
			format,
			mime_type,
			name, uploader,
			EMPTY_SHA512,
			EMPTY_SHA512,
			tag_ids,
			tags,
			created_at,
			first_seen_at,
			updated_at,
			intensities,
			representations,
			source_url,
			view_url
		) {
			OriginalHash = orig_sha512_hash;
			ImageHash    = sha512_hash;
		}

		private Uri GetRepresentation( string mapping ) {
			return Represenstations.ContainsKey( mapping ) ? Represenstations[mapping] : null;
		}
	}
}