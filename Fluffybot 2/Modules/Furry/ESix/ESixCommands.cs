﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using ESixSharp;
using FluffyBot.Utilities;

namespace FluffyBot.Modules.Furry.ESix {
	// ReSharper disable once UnusedType.Global
	[SuppressMessage( "ReSharper", "UnusedMember.Global" )]
	public sealed class ESixCommands : ModuleBase<SocketCommandContext> {
		private const string NO_POST_STR   = "{0}, there is no post for your request.";
		private const string PORN_POST_STR = "{0}, here is your porn you horny.";
		private const string SAFE_POST_STR = "{0}, here is some lovely art.";

		public ESixCommands() {
			_ = ESixHelper.CreateOrGetManager();
		}

		/// <summary>
		/// Grabs a random not safe for work (NSFW) image from e621.net.
		/// </summary>
		/// <returns>A task for Discord.NET</returns>
		[Command( "ESix.RandomImage" )]
		[RequireNsfw]
		public Task RandomAdultImage() {
			if ( !IsEsixManagerAlive() ) {
				return Task.CompletedTask;
			}
			
			var post = GrabPost();

			return CommonPostReturn( post, true );
		}

		/// <summary>
		/// Searches for a random image based on the tags given to it.
		/// </summary>
		/// <param name="tags">The tags to use for a search. Max 6</param>
		/// <returns>A task for Discord.NET</returns>
		[Command( "ESix.ImageSearch" )]
		[RequireNsfw]
		public Task AdultImageTagSearch( params string[] tags ) {
			if ( !IsEsixManagerAlive() ) {
				return Task.CompletedTask;
			}
			
			var post = GrabPost( tags: tags );

			return CommonPostReturn( post, true );
		}

		/// <summary>
		/// Searches for a random safe for work (SFW) image from e926.net.
		/// </summary>
		/// <returns>A task for Discord.NET</returns>
		[Command( "ENine.RandomImage" )]
		public Task RandomSafeImage() {
			if ( !IsEsixManagerAlive() ) {
				return Task.CompletedTask;
			}

			var post = GrabPost( true );

			return CommonPostReturn( post );
		}

		/// <summary>
		/// Searches for a random safe for work (SFW) image using tags.
		/// </summary>
		/// <param name="tags">Tags to use in the search. Max 6</param>
		/// <returns>A task for Discord.NET</returns>
		[Command( "ENine.ImageSearch" )]
		public Task SafeImageTagSearch( params string[] tags ) {
			if ( !IsEsixManagerAlive() ) {
				return Task.CompletedTask;
			}

			var post = GrabPost( true, tags );

			return CommonPostReturn( post );
		}

		/// <summary>
		/// Allows for adding tags to a blacklist. Requires <see cref="IGuild"/> owner.
		/// </summary>
		/// <param name="tags">The tag(s) to blacklist.</param>
		/// <returns>A task for Discord.NET</returns>
		[Command( "ESix.Blacklist.Add" )]
		[Alias( "ENine.Blacklist.Add" )]
		public Task AddTagsToBlacklist( params string[] tags ) {
			if ( !IsEsixManagerAlive() ) {
				return Task.CompletedTask;
			}

			List<string> blacklist;

			Context.Message.DeleteAsync().ConfigureAwait( false ).GetAwaiter().GetResult();

			// Must be Guild owner in order to change the blacklist.
			if ( Context.User.Id != Context.Guild.OwnerId ) {
				return Context.Channel.SendMessageAsync( $"{Context.User.Mention}, you are not allowed to do this, only {Context.Guild.Owner.Mention} can." );
			}

			if ( !SettingsManager.HasSetting( ESixHelper.ESIX_BLACKLIST_KEY, Context.Guild ) ) {
				blacklist = new List<string>();

				SettingsManager.AddSetting( ESixHelper.ESIX_BLACKLIST_KEY, blacklist, Context.Guild );
			} else {
				blacklist = SettingsManager.GetSetting<List<string>>( ESixHelper.ESIX_BLACKLIST_KEY, Context.Guild );
			}

			foreach ( var tag in tags ) {
				var index = blacklist.BinarySearch( tag.ToLower() );

				LogManager.Log( $"Binary search returned `{index}` (bitwise compliment: `{~index}`) for `{tag}`." );

				if ( 0 <= index ) {
					continue;
				}

				blacklist.Insert( ~index, tag );
			}

			SettingsManager.SetSetting( ESixHelper.ESIX_BLACKLIST_KEY, blacklist, Context.Guild );

			return Context.User.SendMessageAsync( "Blacklist updated." );
		}

		/// <summary>
		/// Removes the tag(s) from the <see cref="IGuild"/>s blacklist, if they have one.
		/// </summary>
		/// <param name="tags">The tag(s) to remove from the blacklist.</param>
		/// <returns>A task for Discord.NET</returns>
		[Command( "ESix.Blacklist.Remove" )]
		[Alias( "ENine.Blacklist.Remove" )]
		public Task RemoveTagFromBlacklist( params string[] tags ) {
			if ( !IsEsixManagerAlive() ) {
				return Task.CompletedTask;
			}

			Context.Message.DeleteAsync().ConfigureAwait( false ).GetAwaiter().GetResult();

			// Must be Guild owner to change the blacklist.
			if ( Context.User.Id != Context.Guild.OwnerId ) {
				return Context.Channel.SendMessageAsync( $"{Context.User.Mention}, you are not allowed to do this, only {Context.Guild.Owner.Mention} can." );
			}

			if ( !SettingsManager.HasSetting( ESixHelper.ESIX_BLACKLIST_KEY, Context.Guild ) ) {
				return Context.User.SendMessageAsync( "Failed to change blacklist: Blacklist does not exist." );
			}

			var blacklist = SettingsManager.GetSetting<List<string>>( ESixHelper.ESIX_BLACKLIST_KEY, Context.Guild );

			foreach ( var tag in tags ) {
				var index = blacklist.BinarySearch( tag.ToLower() );

				LogManager.Log( $"`{tag}` location is `{index}` in blacklist." );

				if ( index < 0 ) {
					continue;
				}

				blacklist.RemoveAt( index );
			}

			return Context.User.SendMessageAsync( "Blacklist updated." );
		}

		[Command( "ESix.Blacklist.Show" )]
		[Alias( "ENine.Blacklist.Show" )]
		public Task ShowBlacklist() {
			if ( !IsEsixManagerAlive() ) {
				return Task.CompletedTask;
			}

			Context.Message.DeleteAsync().ConfigureAwait( false ).GetAwaiter().GetResult();

			if ( !SettingsManager.TryGetSetting( ESixHelper.ESIX_BLACKLIST_KEY, Context.Guild, out List<string> blacklist ) ) {
				return Context.User.SendMessageAsync( "There is no blacklist, everything is free game." );
			}

			using var memStream = new MemoryStream();
			using var writer    = new StreamWriter( memStream, Encoding.Unicode, leaveOpen: true );

			foreach ( var tag in blacklist ) {
				writer.WriteLine( tag );
			}

			return Context.User.SendFileAsync( memStream, "Blacklist.txt", "Here is the blacklist." );
		}

		[Command( "ESix.MaxCycles.Set" )]
		[Alias( "ENine.MaxCycles.Set" )]
		public Task SetMaxCycles( int maxCycle ) {
			if ( !IsEsixManagerAlive() ) {
				return Task.CompletedTask;
			}

			Context.Message.DeleteAsync().ConfigureAwait( false ).GetAwaiter().GetResult();

			if ( Context.User.Id != Context.Guild.OwnerId ) {
				return Context.Channel.SendMessageAsync( $"Only {Context.Guild.Owner.Mention} can change the cycles." );
			}

			if ( maxCycle != -1 && maxCycle < 0 ) {
				return Context.User.SendMessageAsync( $"Max cycles must be either -1 or (0, `{int.MaxValue}]`." );
			}

			if ( !SettingsManager.HasSetting( ESixHelper.MAX_CYCLE_KEY, Context.Guild ) ) {
				SettingsManager.AddSetting( ESixHelper.MAX_CYCLE_KEY, maxCycle, Context.Guild );
			} else {
				SettingsManager.SetSetting( ESixHelper.MAX_CYCLE_KEY, maxCycle, Context.Guild );
			}

			return Context.User.SendMessageAsync( $"Successfully set the max cycles to `{maxCycle}`." );
		}

		[Command( "ESix.MaxCycles.Show" )]
		[Alias( "ENine.MaxCycles.Show" )]
		public Task ShowMaxCycles() {
			if ( !IsEsixManagerAlive() ) {
				return Task.CompletedTask;
			}

			Context.Message.DeleteAsync().ConfigureAwait( false ).GetAwaiter().GetResult();

			return Context.User.SendMessageAsync( !SettingsManager.TryGetSetting( ESixHelper.MAX_CYCLE_KEY, Context.Guild, out int maxCycles ) ? "There is no limit, recommend setting one." : $"Max cycle is `{maxCycles}`." );
		}

		private bool IsEsixManagerAlive() {
			if ( ESixManager.CurrentManager != null ) {
				return true;
			}

			Context.Channel.SendMessageAsync( "The e621 module has been disabled." );

			return false;
		}

		private Task CommonPostReturn( Post post, bool nsfw = false ) {
			var message = post is null ? string.Format( NO_POST_STR, Context.User.Mention ) : string.Format( nsfw ? PORN_POST_STR : SAFE_POST_STR, Context.User.Mention );

			return Context.Channel.SendMessageAsync( message, embed: ESixHelper.CreatePostEmbed( post ) );
		}

		// Since a lot of the methods are basically the same, with only minor changes in logic, this method does it all in 1 call.
		private Post GrabPost( bool safe = false, string[] tags = null ) {
			LogManager.Log( "Grabbing posts." );

			if ( !SettingsManager.TryGetSetting( ESixHelper.MAX_CYCLE_KEY, Context.Guild, out int maxCycles ) ) {
				maxCycles = -1;
			}

			Post post;

			try {
				var posts = safe ? GrabSafePosts() : GrabAdultPosts();
				var rand  = new Random();

				do {
					post = posts[rand.Next( 0, posts.Length )];
				} while ( safe && post.Rating != Rating.Safe || !safe && post.Rating == Rating.Safe );
			} catch ( Exception e ) {
				LogManager.LogException( e );

				return null;
			}

			return post;

			// This local method is not static because it is state-sensitive.
			Post[] GrabAdultPosts() {
				if ( !SettingsManager.HasSetting( ESixHelper.ESIX_BLACKLIST_KEY, Context.Guild )
					 || ( SettingsManager.TryGetSetting<List<string>>( ESixHelper.ESIX_BLACKLIST_KEY, Context.Guild, out var blacklist )
						  && blacklist.Count == 0 ) ) {
					var posts = tags is null || tags.Length == 0 ? PostManager.GetAnyPosts( maxCycles ) : PostManager.GetPostsFromTags( maxCycles, tags );

					return posts.All( p => p.Rating == Rating.Safe ) ? null : posts;
				}

				var postEnumerable = tags is null || tags.Length == 0 ? PostManager.EnumerateAnyPosts( maxCycles ) : PostManager.EnumeratePostsFromTags( maxCycles, tags );

				return ESixHelper.BlacklistFilter( postEnumerable, blacklist );
			}

			// This method is not static because it is state-sensitive.
			Post[] GrabSafePosts() {
				if ( !SettingsManager.HasSetting( ESixHelper.ESIX_BLACKLIST_KEY, Context.Guild )
					 || SettingsManager.TryGetSetting<List<string>>( ESixHelper.ESIX_BLACKLIST_KEY, Context.Guild, out var blacklist )
					 && blacklist.Count == 0 ) {
					return tags is null || tags.Length == 0 ? PostManager.GetAnySafePosts( maxCycles ) : PostManager.GetPostsFromSafeTags( maxCycles, tags );
				}

				var postEnumerable = tags is null || tags.Length == 0 ? PostManager.EnumerateAnySafePosts( maxCycles ) : PostManager.EnumeratePostsFromSafeTags( maxCycles, tags );

				return ESixHelper.BlacklistFilter( postEnumerable, blacklist );
			}
		}
	}
}