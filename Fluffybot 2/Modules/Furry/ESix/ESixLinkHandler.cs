using System;
using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using ESixSharp;
using FluffyBot.Utilities;

namespace FluffyBot.Modules.Furry.ESix {
	[SuppressMessage( "ReSharper", "UnusedType.Global" )]
	internal class ESixLinkHandler : LinkHandler {
		private static readonly Regex CDN_REGEX     = new Regex( @"(?:http(?:s)?:\/\/)?static1\.e(?:621|926)\.net\/data\/[a-zA-Z0-9]{2}\/[a-zA-Z0-9]{2}\/([a-zA-Z0-9]{32})\.(?:png|jpeg|jpg|gif|webm)" );
		private static readonly Regex WEBSITE_REGEX = new Regex( @"(?:http(?:s)?:\/\/)?e(?:621|926)\.net\/posts\/[0-9]+" );

		private static Task ProcessCDNLink( string link, string md5, IMentionable user, ISocketMessageChannel channel ) {
			_ = ESixHelper.CreateOrGetManager();
			var post  = PostManager.GetPostsFromTags( -1, $"md5:{md5}" )[0];
			var embed = ESixHelper.CreatePostEmbed( post );

			return channel?.SendMessageAsync( $"{user.Mention}, here is a better look at `{link}`.", embed: embed );
		}

		private static Task ProcessWebLink( string link, IMentionable user, ISocketMessageChannel channel ) {
			_ = ESixHelper.CreateOrGetManager();
			var post  = PostManager.GetPostFromLink( link );
			var embed = ESixHelper.CreatePostEmbed( post );

			return channel?.SendMessageAsync( $"{user.Mention}, here is a better look at `{link}`.", embed: embed );
		}

		public ESixLinkHandler() : base(
			new[] {
				new Uri( "https://e621.net" ),
				new Uri( "https://e926.net" ),
				new Uri( "https://www.e621.net" ),
				new Uri( "https://www.e926.net" ),
				new Uri( "https://static1.e621.net" ),
				new Uri( "https://static1.e926.net" ),
			}
		) {}

		public override bool ContainsValidLink( string message ) => CDN_REGEX.IsMatch( message ) || WEBSITE_REGEX.IsMatch( message );

		protected override void ProcessLink( string message, IMentionable user, ISocketMessageChannel channel ) {
			if ( CDN_REGEX.IsMatch( message ) ) {
				foreach ( Match match in CDN_REGEX.Matches( message ) ) {
					try {
						ProcessCDNLink( match.Value, match.Groups[1].Value, user, channel )
							.ConfigureAwait( false )
							.GetAwaiter()
							.GetResult();
					} catch ( Exception e ) {
						LogManager.LogException( e );
					}
				}
			}

			if ( !WEBSITE_REGEX.IsMatch( message ) ) {
				return;
			}

			foreach ( Match match in WEBSITE_REGEX.Matches( message ) ) {
				try {
					ProcessWebLink( match.Value, user, channel )
						.ConfigureAwait( false )
						.GetAwaiter()
						.GetResult();
				} catch ( Exception e ) {
					LogManager.LogException( e );
				}
			}
		}
	}
}