using System;
using System.Collections.Generic;
using System.Linq;
using Discord;
using ESixSharp;
using FluffyBot.Utilities;

namespace FluffyBot.Modules.Furry.ESix {
	/// <summary>
	/// This class is intended to help with any e621/926 actions and keep it separate from <see cref="ESixCommands"/>.
	/// </summary>
	internal static class ESixHelper {
		private const string ESIX_API_KEY = "e621.apiKey";
		
		internal const string ESIX_BLACKLIST_KEY = "e621.blacklist";
		internal const string MAX_CYCLE_KEY      = "e621.maxCycles";
		internal const string BOT_RUNNER_NAME    = "fluffybot.owner";

		internal static Embed CreatePostEmbed( Post post ) {
			if ( post is null ) {
				return null;
			}

			var embedBuilder = new EmbedBuilder {
				Color        = Color.DarkBlue,
				Description  = post.Description.Length > 1023 ? post.Description.Substring( 0, 1023 ) : post.Description,
				ImageUrl     = post.File.URL.ToString(),
				ThumbnailUrl = post.Preview.URL.ToString(),
				Url          = post.PostURL.ToString(),
				Timestamp    = post.Updated,
			};

			embedBuilder.AddField( "Score", post.Score.Total, true );
			embedBuilder.AddField( "Width", post.File.Width, true );
			embedBuilder.AddField( "Height", post.File.Height, true );
			embedBuilder.AddField( "File Type", post.File.Extension, true );
			embedBuilder.AddField( "File Size", post.File.Size, true );
			embedBuilder.AddField( "Link", post.PostURL.ToString(), true );

			return embedBuilder.Build();
		}

		internal static Post[] BlacklistFilter( IEnumerable<Post> postEnumerable, IList<string> blacklist ) {
			var postList = new List<Post>();

			foreach ( var post in postEnumerable ) {
				var postTags = post.Tags.AllTags.ToArray();
				var skip     = blacklist.Any( tag => postTags.Contains( tag ) );

				if ( skip ) {
					continue;
				}
				
				postList.Add( post );
			}

			return postList.ToArray();
		}

		internal static ESixManager CreateOrGetManager() {
			try {
				return ESixManager.CurrentManager;
			} catch {
				if ( !SettingsManager.TryGetSetting( BOT_RUNNER_NAME, out string username ) ) {
					return null;
				}

				if ( !SettingsManager.TryGetSetting( ESIX_API_KEY, out string apiKey ) ) {
					LogManager.LogWarning( "Unable to grab API key, please provide one to have unrestricted tag access. Without it, the bot my freeze on attempting to grab posts." );
				}
				
				return ESixManager.Create( "FluffyBot-v2", username, apiKey );
			}
		}
	}
}