using System;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using FluffyBot.Utilities;

namespace FluffyBot.Modules {
	public abstract class LinkHandler {
		// Needs to be protected to have children access it.
		// ReSharper disable once MemberCanBePrivate.Global
		protected readonly Uri[] domains;

		protected LinkHandler( Uri[] domains ) {
			this.domains = domains;
		}

		public void ProcessLinkAsync( string message, IMentionable user, ISocketMessageChannel channel ) => Task.Run(
			async () => {
				try {
					await Task.Run( () => ProcessLink( message, user, channel ) ).ConfigureAwait( false );
				} catch ( Exception e ) {
					LogManager.LogException( $"Something happened while attempting to handle a link.", e );
				}
			}
		);

		public abstract bool ContainsValidLink( string message );

		protected abstract void ProcessLink( string message, IMentionable user, ISocketMessageChannel channel );
	}
}