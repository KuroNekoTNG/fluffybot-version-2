﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage(
	"Style", "IDE1006:Naming Styles",
	Justification = "This is how it looks for Linux, which uses C/C++ and not C#.",
	Scope         = "member", Target = "~M:FluffyBot.Nix.LibC.getuid~System.Int32"
)]
[assembly: SuppressMessage(
	"Style", "IDE1006:Naming Styles",
	Justification = "This is how it looks for Linux, which uses C/C++ and not C#.",
	Scope         = "member", Target = "~M:FluffyBot.Nix.LibC.getgid~System.Int32"
)]
[assembly: SuppressMessage(
	"Style", "IDE1006:Naming Styles",
	Justification = "This is how it looks for Linux, which uses C/C++ and not C#.",
	Scope         = "member", Target = "~M:FluffyBot.Nix.LibC.setuid(System.Int32)~System.Int32"
)]
[assembly: SuppressMessage(
	"Style", "IDE1006:Naming Styles",
	Justification = "This is how it looks for Linux, which uses C/C++ and not C#.",
	Scope         = "member", Target = "~M:FluffyBot.Nix.LibC.setgid(System.Int32)~System.Int32"
)]
[assembly: SuppressMessage(
	"Style", "IDE1006:Naming Styles",
	Justification = "This is how it looks for Linux, which uses C/C++ and not C#.",
	Scope         = "member", Target = "~M:FluffyBot.Nix.LibC.chmod(System.String,System.UInt32)~System.Int32"
)]
[assembly: SuppressMessage(
	"Style", "IDE1006:Naming Styles",
	Justification = "This is how it looks for Linux, which uses C/C++ and not C#.",
	Scope         = "member", Target = "~M:FluffyBot.Nix.LibC.chown(System.String,System.UInt32,System.UInt32)~System.Int32"
)]