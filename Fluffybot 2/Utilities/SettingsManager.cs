﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Discord;
using FluffyBot.Attributes;
using Newtonsoft.Json;

namespace FluffyBot.Utilities {
	[RunStaticConstructor]
	[SuppressMessage( "ReSharper", "UnusedMember.Global" )]
	[SuppressMessage( "ReSharper", "MemberCanBePrivate.Global" )]
	public static class SettingsManager {
		public static readonly string SETTING_FOLDER;

		private static readonly object LOCK_OBJ = new object();

		private static readonly JsonSerializerSettings JSON_OPTIONS = new JsonSerializerSettings() {
			ConstructorHandling   = ConstructorHandling.AllowNonPublicDefaultConstructor,
			DateTimeZoneHandling  = DateTimeZoneHandling.Utc,
			FloatParseHandling    = FloatParseHandling.Double,
			ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
			MissingMemberHandling = MissingMemberHandling.Ignore,
			DateParseHandling     = DateParseHandling.DateTimeOffset,
			DateFormatHandling    = DateFormatHandling.IsoDateFormat,
			FloatFormatHandling   = FloatFormatHandling.Symbol,
			NullValueHandling     = NullValueHandling.Ignore,
			DefaultValueHandling  = DefaultValueHandling.Ignore,
			StringEscapeHandling  = StringEscapeHandling.EscapeNonAscii,
			TypeNameHandling      = TypeNameHandling.All,
		};

		[SuppressMessage( "ReSharper", "MemberCanBePrivate.Global" )]
		public static string MainSettingFile { get; }

		private static Dictionary<string, object>                    globalSettings = new Dictionary<string, object>();
		private static Dictionary<ulong, Dictionary<string, object>> guildSettings  = new Dictionary<ulong, Dictionary<string, object>>();

		[SuppressMessage( "ReSharper", "FunctionNeverReturns" )]
		static SettingsManager() {
			var envPath = Environment.GetEnvironmentVariable( "FB_DEBUG_SETTING_LOCATION" );

			if ( string.IsNullOrWhiteSpace( envPath ) ) {
				if ( RuntimeInformation.IsOSPlatform( OSPlatform.Windows ) ) {
					SETTING_FOLDER = @"C:\ProgramData\FluffyBot\Settings";
				} else if ( RuntimeInformation.IsOSPlatform( OSPlatform.Linux ) || RuntimeInformation.IsOSPlatform( OSPlatform.FreeBSD ) ) {
					SETTING_FOLDER = "/etc/fluffybot";
				} else {
					Path.Combine( Environment.CurrentDirectory, "settings" );
				}
			} else {
				SETTING_FOLDER = envPath;
			}

			MainSettingFile = Path.Combine( SETTING_FOLDER!, "global.json" );

			if ( !Directory.Exists( SETTING_FOLDER ) ) {
				_ = Directory.CreateDirectory( SETTING_FOLDER );
			}

			if ( !File.Exists( MainSettingFile ) ) {
				File.Create( MainSettingFile ).Dispose();
			}

			try {
				LoadFromFile();
			} catch ( Exception e ) {
				LogManager.LogException( e );
			}

			var settingsWatcher = new FileSystemWatcher( SETTING_FOLDER, "*.json" ) {
				EnableRaisingEvents   = true,
				IncludeSubdirectories = false
			};

			settingsWatcher.Changed += ( _, e ) => LoadFromFile();

			_ = Task.Run(
				async () => {
					int globalNum;
					int guildSettingsCount;
					var minDelay           = new TimeSpan( 0, 1, 0 );
					var guildSettingCounts = new List<(ulong id, int count)>();

					UpdateValues();

					while ( true ) {
						await Task.Delay( minDelay ).ConfigureAwait( false );

						if ( globalNum == globalSettings.Count && guildSettingsCount == guildSettings.Count && !GuildSettingsChanged() ) {
							continue;
						}

						settingsWatcher.EnableRaisingEvents = false;

						SaveToFile();
						UpdateValues();

						settingsWatcher.EnableRaisingEvents = true;
					}

					void UpdateValues() {
						lock ( LOCK_OBJ ) {
							var pairs = new List<KeyValuePair<ulong, Dictionary<string, object>>>( guildSettings );

							globalNum          = globalSettings.Count;
							guildSettingsCount = guildSettings.Count;

							for ( var i = 0; i < pairs.Count; ++i ) {
								if ( i < guildSettingCounts.Count ) {
									guildSettingCounts[i] = ( pairs[i].Key, pairs[i].Value.Count );
								} else {
									guildSettingCounts.Add( ( pairs[i].Key, pairs[i].Value.Count ) );
								}
							}
						}
					}

					bool GuildSettingsChanged() {
						lock ( LOCK_OBJ ) {
							for ( var i = 0; i < guildSettingsCount; ++i ) {
								var (id, count) = guildSettingCounts[i];

								if ( guildSettings[id].Count != count ) {
									return true;
								}
							}
						}

						return false;
					}
				}
			);
		}

		#region Global Settings

		public static bool HasSetting( string key ) => globalSettings.ContainsKey( key );

		public static void AddSetting( string key, object value ) {
			lock ( LOCK_OBJ ) {
				globalSettings.Add( key, value );
			}
		}

		public static bool TryAddSettings( string key, object value ) {
			bool status;

			lock ( LOCK_OBJ ) {
				status = globalSettings.TryAdd( key, value );
			}

			return status;
		}

		public static void SetSetting( string key, object value ) {
			lock ( LOCK_OBJ ) {
				globalSettings[key] = value;
			}
		}

		public static bool TrySetSetting( string key, object value ) {
			lock ( LOCK_OBJ ) {
				if ( !globalSettings.ContainsKey( key ) ) {
					return false;
				}

				globalSettings[key] = value;
			}

			return true;
		}

		public static object GetSetting( string key ) {
			lock ( LOCK_OBJ ) {
				return globalSettings[key];
			}
		}

		public static T GetSetting<T>( string key ) {
			var objVal = GetSetting( key );

			return ( T ) Convert.ChangeType( objVal, typeof( T ) );
		}

		public static bool TryGetSetting( string key, out object value ) {
			lock ( LOCK_OBJ ) {
				return globalSettings.TryGetValue( key, out value );
			}
		}

		public static bool TryGetSetting<T>( string key, out T value ) {
			value = default;

			if ( !TryGetSetting( key, out var objValue ) ) {
				return false;
			}

			try {
				value = ( T ) Convert.ChangeType( objValue, typeof( T ) );
			} catch ( Exception e ) {
				LogManager.LogException( e );

				return false;
			}

			return true;
		}

		#endregion

		#region Guild Settings

		public static bool HasSetting( string key, IGuild guild ) {
			if ( !guildSettings.ContainsKey( guild.Id ) ) {
				return false;
			}

			return guildSettings[guild.Id].ContainsKey( key );
		}

		public static void AddSetting( string key, object value, IGuild guild ) {
			lock ( LOCK_OBJ ) {
				Dictionary<string, object> settings;
				if ( !guildSettings.ContainsKey( guild.Id ) ) {
					settings = new Dictionary<string, object>();

					guildSettings.Add( guild.Id, settings );
				} else {
					settings = guildSettings[guild.Id];
				}

				settings.Add( key, value );
			}
		}

		public static bool TryAddSetting( string key, object value, IGuild guild ) {
			try {
				AddSetting( key, value, guild );

				return true;
			} catch ( Exception e ) {
				LogManager.LogException( e );

				return false;
			}
		}

		public static void SetSetting( string key, object value, IGuild guild ) {
			lock ( LOCK_OBJ ) {
				Dictionary<string, object> settings;
				if ( !guildSettings.ContainsKey( guild.Id ) ) {
					settings = new Dictionary<string, object>();

					guildSettings.Add( guild.Id, settings );
				} else {
					settings = guildSettings[guild.Id];
				}

				settings[key] = value;
			}
		}

		public static bool TrySetSetting( string key, object value, IGuild guild ) {
			try {
				SetSetting( key, value, guild );

				return true;
			} catch ( Exception e ) {
				LogManager.LogException( e );

				return false;
			}
		}

		public static object GetSetting( string key, IGuild guild ) {
			lock ( LOCK_OBJ ) {
				return guildSettings[guild.Id][key];
			}
		}

		public static T GetSetting<T>( string key, IGuild guild ) {
			lock ( LOCK_OBJ ) {
				var tObj = guildSettings[guild.Id][key];

				return ( T ) Convert.ChangeType( tObj, typeof( T ) );
			}
		}

		public static bool TryGetSetting( string key, IGuild guild, out object value ) {
			value = default;

			try {
				value = GetSetting( key, guild );

				return true;
			} catch ( Exception e ) {
				LogManager.LogException( e );

				return false;
			}
		}

		public static bool TryGetSetting<T>( string key, IGuild guild, out T value ) {
			value = default;

			try {
				value = GetSetting<T>( key, guild );

				return true;
			} catch ( Exception e ) {
				LogManager.LogException( e );

				return false;
			}
		}

		#endregion

		public static string GetSettingFile( IGuild guild ) => Path.Combine( SETTING_FOLDER, $"{guild.Id}.json" );

		private static void LoadFromFile() {
			lock ( LOCK_OBJ ) {
				foreach ( var settingFile in Directory.EnumerateFiles( SETTING_FOLDER, "*.json", SearchOption.TopDirectoryOnly ) ) {
					LoadFromJson( settingFile );
				}
			}

			static void LoadFromJson( string path ) {
				string json;
				var    id                = 0uL;
				var    start             = path.LastIndexOf( Path.DirectorySeparatorChar ) + 1;
				var    length            = path.LastIndexOf( ".json", StringComparison.Ordinal ) - start;
				var    globalSettingFile = path.Equals( MainSettingFile, StringComparison.OrdinalIgnoreCase );

				if ( !globalSettingFile && !ulong.TryParse( path.Substring( start, length ), out id ) ) {
					return;
				}

				using ( var fileStream = new FileStream( path, FileMode.Open, FileAccess.Read, FileShare.Read ) ) {
					using ( var settingReader = new StreamReader( fileStream, Encoding.UTF8 ) ) {
						json = settingReader.ReadToEnd();
					}
				}

				var settings = JsonConvert.DeserializeObject<Dictionary<string, object>>( json, JSON_OPTIONS ); //JsonSerializer.Deserialize<Dictionary<string, object>>( json, JSON_OPTIONS );

				if ( settings is null ) {
					return;
				}

				if ( globalSettingFile ) {
					globalSettings = settings;
				} else {
					guildSettings ??= new Dictionary<ulong, Dictionary<string, object>>();

					if ( guildSettings.ContainsKey( id ) ) {
						guildSettings[id] = settings;
					} else {
						guildSettings.Add( id, settings );
					}
				}
			}
		}

		private static void SaveToFile() {
			lock ( LOCK_OBJ ) {
				var json = JsonConvert.SerializeObject( globalSettings, JSON_OPTIONS );

				using ( var globalStream = new FileStream( MainSettingFile, FileMode.Create, FileAccess.Write, FileShare.Read ) ) {
					using ( var globalWriter = new StreamWriter( globalStream, Encoding.UTF8 ) ) {
						globalWriter.Write( json );
					}
				}

				foreach ( var (key, value) in guildSettings ) {
					using var guildSettingStream = new FileStream( Path.Combine( SETTING_FOLDER, $"{key}.json" ), FileMode.Create, FileAccess.Write, FileShare.Read );
					using var guildSettingWriter = new StreamWriter( guildSettingStream, Encoding.UTF8 );

					json = JsonConvert.SerializeObject( globalSettings, JSON_OPTIONS );

					guildSettingWriter.Write( json );
				}
			}
		}
	}
}