﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
#if !DEBUG
using System.Runtime.InteropServices;
#endif
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FluffyBot.Attributes;
using Newtonsoft.Json;

namespace FluffyBot.Utilities {
	/// <summary>
	/// Manages all of the logging for the program.
	/// </summary>
	[RunStaticConstructor]
	public static class LogManager {
		private enum Severity : byte {
			Normal    = 0,
			Warning   = 1,
			Error     = 2,
			Exception = 3,
		}

		[SuppressMessage( "ReSharper", "UnusedMember.Local" )]
		private class LogMessage {
			[JsonIgnore]
			private readonly StackTrace _stackTrace;

			/// <summary>
			/// The severity of the log information.
			/// </summary>
			public Severity Severity {
				get;
				set;
			}

			/// <summary>
			/// The message of the log.
			/// </summary>
			public string Message {
				get;
				set;
			}

			public string StackTrace {
				get {
					if ( Exception is Exception e ) {
						return e.StackTrace;
					}

					if ( _stackTrace is null ) {
						throw new NullReferenceException( "StackTrace or Exception is not set, there for a StackTrace cannot be returned." );
					}

					return _stackTrace.ToString();
				}
			}

			/// <summary>
			/// The exception that rides along with the rest of the log.
			/// </summary>
			public Exception Exception {
				get;
				set;
			}

			public LogMessage() : this( string.Empty, null, new StackTrace( 3 ) ) {}

			public LogMessage( string message ) : this( message, null, new StackTrace( 3 ) ) {}

			public LogMessage( string message, StackTrace stackTrace ) : this( message, null, stackTrace ) {}

			public LogMessage( string message, Severity severity ) : this( message, severity, null, new StackTrace( 3 ) ) {}

			public LogMessage( string message, Severity severity, StackTrace stackTrace ) : this( message, severity, null, stackTrace ) {}

			public LogMessage( Exception e ) : this( $"Exception `{e.GetType().FullName}` has occured.", Severity.Exception, e ) {}

			public LogMessage( Severity severity, Exception e ) : this( $"Exception `{e.GetType().FullName}` has occured.", severity, e ) {}

			public LogMessage( string message, Exception e ) : this( message, Severity.Exception, e ) {}

			public LogMessage( Exception e, StackTrace stackTrace ) : this( $"Exception `{e.GetType().FullName}` has occured.", e, stackTrace ) {}

			public LogMessage( Severity severity, Exception e, StackTrace stackTrace ) : this( $"Exception `{e.GetType().FullName}` has occured.", severity, e, stackTrace ) {}

			public LogMessage( string message, Exception e, StackTrace stackTrace ) : this( message, Severity.Normal, e, stackTrace ) {}

			public LogMessage( string message, Severity severity, Exception e, StackTrace stackTrace = null ) {
				Message     = message;
				Exception   = e;
				Severity    = severity;
				_stackTrace = stackTrace;
			}
		}

		/// <summary>
		/// The location of the log file.
		/// </summary>
		public static readonly string MAIN_LOG_FOLDER;

		private static readonly ManualResetEvent            newLog = new ManualResetEvent( false );
		private static readonly CancellationTokenSource     source = new CancellationTokenSource();
		private static readonly ConcurrentQueue<LogMessage> queue  = new ConcurrentQueue<LogMessage>();

		/// <summary>
		/// The location of the main log file.
		/// </summary>
		[SuppressMessage( "ReSharper", "MemberCanBePrivate.Global" )]
		public static string CoreLogFile {
			get;
		}

		/// <summary>
		/// If it should also print the log to the console as well.
		/// </summary>
		private static bool PrintToConsole {
			get;
		} = Environment.GetCommandLineArgs().Contains( "--print-to-console" );

		static LogManager() {
			#if DEBUG
			MAIN_LOG_FOLDER = ".";
			#else
			/* Yes I know that I have excluded macOS from this tree, but hear me out for my reason.
			 * No one would be running a macOS server that has this bot also running, it just would not make sense.
			 * I know that macOS is Unix/based on BSD and "there for because I included FreeBSD I should include macOS" is dumb.
			 * macOS is designed to be a user-focused OS platform, not a server platform that this bot should be running on.
			 * Windows is included because it does have a server version that quite a lot of people use.
			 * Linux is included because it is Free and Open Source and it is used for servers.
			 * FreeBSD is also included because it to is used in quite a bit of servers as well.
			 * macOS Server, just remembered that it is a thing and I cannot get it without Apple and Apple is a piece of shit company as well. */
			if ( RuntimeInformation.IsOSPlatform( OSPlatform.Windows ) ) {
				MAIN_LOG_FOLDER = @"C:\ProgramData\FluffyBot\Logs";
			} else if ( RuntimeInformation.IsOSPlatform( OSPlatform.Linux ) || RuntimeInformation.IsOSPlatform( OSPlatform.FreeBSD ) ) {
				MAIN_LOG_FOLDER = "/var/log/fluffybot";
			} else {
				MAIN_LOG_FOLDER = Path.Combine( Environment.CurrentDirectory, "logs" );
			}
			#endif

			CoreLogFile = Path.Combine( MAIN_LOG_FOLDER, "core.log.json" );

			if ( !Directory.Exists( MAIN_LOG_FOLDER ) ) {
				_ = Directory.CreateDirectory( MAIN_LOG_FOLDER );
			}

			var logFileStream = new FileStream( CoreLogFile, FileMode.Create, FileAccess.Write, FileShare.Read );
			var token         = source.Token;

			var logStream = new StreamWriter( logFileStream, Encoding.Unicode );

			var loggingTask = Task.Run(
				() => {
					var entry = 0;

					logStream.Write( "[]" );
					logStream.Flush();

					// This is to ensure that the task never stops running.
					while ( !token.IsCancellationRequested ) {
						newLog.WaitOne();

						logStream.BaseStream.Position -= 2;

						// Great way to get all of the log messages even if new ones are added.
						while ( queue.TryDequeue( out var log ) ) {
							// The log is in a JSON format.
							var json = JsonConvert.SerializeObject( log );

							if ( entry > 0 ) {
								logStream.Write( ',' );
							}

							logStream.Write( json );

							// This is mainly for debugging reasons.
							if ( PrintToConsole ) {
								var builder = new StringBuilder();

								builder.Append( "Message:\t" )
									   .AppendLine( log.Message )
									   .Append( "Severity:\t" )
									   .AppendLine( Enum.GetName( typeof( Severity ), log.Severity ) )
									   .AppendLine( "Log Call Stacktrace:" )
									   .AppendLine( log.StackTrace );

								// Builds a list of exceptions and their stack trace.
								if ( log.Exception?.InnerException != null ) {
									var inner = log.Exception.InnerException;

									do {
										builder.Append( '-', 15 )
											   .Append( "INNER EXCEPTION" )
											   .Append( '-', 15 )
											   .AppendLine()
											   .Append( "Message:\t" )
											   .AppendLine( inner.Message )
											   .Append( "Exception:\t" )
											   .AppendLine( inner.GetType().FullName )
											   .AppendLine( "Stacktrace:" )
											   .AppendLine( log.StackTrace );

										inner = inner.InnerException;
									} while ( inner != null );
								}

								var outputStr = builder.ToString();
								Console.WriteLine( outputStr );
								#if DEBUG
								Debug.WriteLine( outputStr );
								#endif
							}

							++entry;
						}

						logStream.Write( ']' );
						logStream.Flush();

						newLog.Reset();
					}
				}
			);

			NativeHelper.AddCloseHandler(
				() => {
					source.Cancel();

					loggingTask.Wait();

					logStream.Close();
					logStream.Dispose();
				}
			);
		}

		/// <summary>
		/// Logs a message out.
		/// </summary>
		/// <remarks>
		/// When a message is logged using this method, it will be marked as Normal.
		/// </remarks>
		/// <param name="message">The message to log.</param>
		public static void Log( string message ) => LogMessageToQueue( new LogMessage( message ) );

		/// <summary>
		/// Logs out an error message.
		/// </summary>
		/// <remarks>
		/// Error messages are something that was unexpected and that was not expected behaviour.
		/// </remarks>
		/// <param name="message">The error message.</param>
		public static void LogError( string message ) => LogMessageToQueue( new LogMessage( message, Severity.Error ) );

		/// <summary>
		/// Logs out a warning message.
		/// </summary>
		/// <remarks>
		/// Warnings are something that has happened that should be fixed, but is not as important as an error message.
		/// </remarks>
		/// <param name="message"></param>
		public static void LogWarning( string message ) => LogMessageToQueue( new LogMessage( message, Severity.Warning ) );

		/// <summary>
		/// Sends an <see cref="Exception"/> out to the log file.
		/// </summary>
		/// <remarks>
		/// This is when an exception is thrown and this is something that was not expected but was caught.
		/// It is similar to an error, but this is when there is an exception that was caught.
		/// </remarks>
		/// <seealso cref="LogException(string, Exception)"/>
		/// <param name="e">The exception that was caught.</param>
		public static void LogException( Exception e ) => LogMessageToQueue( new LogMessage( Severity.Exception, e ) );

		/// <summary>
		/// Sends a message with the <see cref="Exception"/> out to the log file.
		/// </summary>
		/// <seealso cref="LogException(Exception)"/>
		/// <param name="message">A message explaining what happened when the exception occured.</param>
		/// <param name="e">The <see cref="Exception"/> that was caught.</param>
		public static void LogException( string message, Exception e ) => LogMessageToQueue( new LogMessage( message, Severity.Exception, e ) );

		// Since a lot of the methods above do nearly the same thing, just set up LogMessage differently,
		// this method is called to not rewrite the same code over and over again.
		private static void LogMessageToQueue( LogMessage logMsg ) {
			queue.Enqueue( logMsg );
			newLog.Set();
		}
	}
}