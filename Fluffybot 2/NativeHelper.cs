﻿using System;
using System.Runtime.Loader;
using FluffyBot.WinAPI;

namespace FluffyBot {
	public static class NativeHelper {
		public static void AddCloseHandler( Action handler ) {
			#if NET_WIN_DOCKER
			Kernel32.AddHandler( _ => handler.Invoke() );
			#else
			// ReSharper disable once SwitchStatementHandlesSomeKnownEnumValuesWithDefault
			switch ( Environment.OSVersion.Platform ) {
				case PlatformID.Win32NT:
					Kernel32.AddHandler( _ => handler.Invoke() );
					break;
				default:
					var loadContext = AssemblyLoadContext.GetLoadContext( typeof( NativeHelper ).Assembly );

					loadContext!.Unloading += _ => handler.Invoke();
					break;
			}
			#endif
		}
	}
}