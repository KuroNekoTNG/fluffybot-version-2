﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Runtime.InteropServices;
using FluffyBot.Utilities;

namespace FluffyBot.Nix {
	/// <summary>
	/// Represents certain calls into `libc.so` in Linux/Unix.
	/// </summary>
	[SuppressMessage( "ReSharper", "MemberCanBePrivate.Global" )]
	[SuppressMessage( "ReSharper", "UnusedMember.Global" )]
	public static class LibC {
		/// <summary>
		/// Linux Permission flags when running within the Linux Kernel.
		/// </summary>
		/// <remarks>
		/// These are the flags used by the linux kernel for file permissions.
		/// The flags here are derived from the Manual page using the command:
		/// <code>man 2 chmod</code>
		/// </remarks>
		[Flags]
		public enum Modifiers : uint {
			/// <summary>
			/// Allows the process to set the user ID of the process. In short, it can allow the process to "drop down" into a safer user.
			/// </summary>
			SetUserID = 0b0000_1000_0000_0000, // 04000 - Set Process User ID

			/// <summary>
			/// Allows the process to set the group ID of the process. In short, it can allow the process to "drop down" into a safer group.
			/// </summary>
			SetGroupID = 0b0000_0100_0000_0000, //0b0000_0100_0000_0000, // 02000 - Set Process Group ID

			/// <summary>
			/// Only the owner or root can delete or rename the file or directory.
			/// </summary>
			SetStickyBit = 0b0000_0010_0000_0000, // 01000 - Set Sticky Bit

			/// <summary>
			/// Allows the owner of the file/folder can read the contents.
			/// </summary>
			OwnerRead = 0b0000_0001_0000_0000, // 00400 - Read By Owner Flag

			/// <summary>
			/// Allows the owner to write to the file/folder.
			/// </summary>
			OwnerWrite = 0b0000_0000_1000_0000, // 00200 - Write By Owner Flag

			/// <summary>
			/// Allows the owner to execute the file, or for a program running as the user to view the contents of the folder.
			/// </summary>
			OwnerExecute = 0b0000_0000_0100_0000, // 00100 - Execute By Owner Flag

			/// <summary>
			/// Allows the group to read the contents of the file or folder.
			/// </summary>
			GroupRead = 0b0000_0000_0010_0000, // 00040 - Read By Group Flag

			/// <summary>
			/// Allows the group to write the contents of the file or folder.
			/// </summary>
			GroupWrite = 0b0000_0000_0001_0000, // 00020 - Write By Group Flag

			/// <summary>
			/// Allows the group to execute the file.
			/// </summary>
			GroupExecute = 0b0000_0000_0000_1000, // 00010 - Execute By Group Flag

			/// <summary>
			/// Allows anyone else to read the file or folder.
			/// </summary>
			OtherRead = 0b0000_0000_0000_0100, // 00004 - Read By Other Flag

			/// <summary>
			/// Allows anyone else to write to the file or folder.
			/// </summary>
			OtherWrite = 0b0000_0000_0000_0010, // 00002 - Write By Other Flag

			/// <summary>
			/// Allows anyone to execute the file.
			/// </summary>
			/// <remarks>
			/// It is advised to never set this unless it is on a directory.
			/// Doing so on a file can open the system up to security vulnerabilities.
			/// </remarks>
			OtherExecute = 0b0000_0000_0000_0001, // 00001 - Execute By Other Flag
		}

		/// <summary>
		/// The value that tells Lib C to not change either the owner or group.
		/// </summary>
		private const int DONT_CHANGE = -1;

		private const string LIBRARY = "libc";

		/// <summary>
		/// The user ID that the process is running under.
		/// </summary>
		public static int UserID {
			get => getuid();
			set => setuid( value );
		}

		/// <summary>
		/// The group ID that the process is running under.
		/// </summary>
		public static int GroupID {
			get => getgid();
			set => setgid( value );
		}

		#region Native Linux Calls

		[DllImport( LIBRARY )]
		private static extern int getuid();

		[DllImport( LIBRARY )]
		private static extern int getgid();

		[DllImport( LIBRARY )]
		private static extern int setuid( int uid );

		[DllImport( LIBRARY )]
		private static extern int setgid( int gid );

		[DllImport( LIBRARY )]
		private static extern int chmod( [MarshalAs( UnmanagedType.LPStr )] string path, uint mode );

		[DllImport( LIBRARY )]
		private static extern int chown( [MarshalAs( UnmanagedType.LPStr )] string path, uint owner, uint group );

		#endregion

		/// <summary>
		/// Changes the owner of the file or directory.
		/// </summary>
		/// <seealso cref="ChangeOwner(Uri, int, bool)"/>
		/// <seealso cref="ChangeOwner(FileSystemInfo, int, bool)"/>
		/// <seealso cref="ChangeGroup(string, int, bool)"/>
		/// <seealso cref="ChangeGroup(Uri, int, bool)"/>
		/// <seealso cref="ChangeGroup(FileSystemInfo, int, bool)"/>
		/// <seealso cref="ChangeOwnerAndGroup(string, int, int, bool)"/>
		/// <seealso cref="ChangeOwnerAndGroup(Uri, int, int, bool)"/>
		/// <seealso cref="ChangeOwnerAndGroup(FileSystemInfo, int, int, bool)"/>
		/// <param name="path">The path to the target.</param>
		/// <param name="owner">The numerical value of the owner.</param>
		/// <param name="followTree">Rather to go deeper into the directory or not.</param>
		/// <returns><see langword="true"/> if all succeeded, otherwise <see langword="false"/>.</returns>
		public static bool ChangeOwner( string path, int owner, bool followTree = false ) => ChangeOwnerAndGroup( path, owner, DONT_CHANGE, followTree );

		/// <summary>
		/// Changes the owner of the file or directory.
		/// </summary>
		/// <seealso cref="ChangeOwner(string, int, bool)"/>
		/// <seealso cref="ChangeOwner(FileSystemInfo, int, bool)"/>
		/// <seealso cref="ChangeGroup(string, int, bool)"/>
		/// <seealso cref="ChangeGroup(Uri, int, bool)"/>
		/// <seealso cref="ChangeGroup(FileSystemInfo, int, bool)"/>
		/// <seealso cref="ChangeOwnerAndGroup(string, int, int, bool)"/>
		/// <seealso cref="ChangeOwnerAndGroup(Uri, int, int, bool)"/>
		/// <seealso cref="ChangeOwnerAndGroup(FileSystemInfo, int, int, bool)"/>
		/// <param name="path">The path to the target.</param>
		/// <param name="owner">The numerical value of the owner.</param>
		/// <param name="followTree">Rather to go deeper into the directory or not.</param>
		/// <returns><see langword="true"/> if all succeeded, otherwise <see langword="false"/>.</returns>
		public static bool ChangeOwner( Uri path, int owner, bool followTree = false ) => ChangeOwner( path.AbsolutePath, owner, followTree );

		/// <summary>
		/// Changes the owner of the file or directory.
		/// </summary>
		/// <seealso cref="ChangeOwner(string, int, bool)"/>
		/// <seealso cref="ChangeOwner(Uri, int, bool)"/>
		/// <seealso cref="ChangeGroup(string, int, bool)"/>
		/// <seealso cref="ChangeGroup(Uri, int, bool)"/>
		/// <seealso cref="ChangeGroup(FileSystemInfo, int, bool)"/>
		/// <seealso cref="ChangeOwnerAndGroup(string, int, int, bool)"/>
		/// <seealso cref="ChangeOwnerAndGroup(Uri, int, int, bool)"/>
		/// <seealso cref="ChangeOwnerAndGroup(FileSystemInfo, int, int, bool)"/>
		/// <param name="fsInfo">The <see cref="FileSystemInfo"/> object representing the file or folder.</param>
		/// <param name="owner">The numerical value of the owner.</param>
		/// <param name="followTree">Rather to go deeper into the directory or not.</param>
		/// <returns><see langword="true"/> if all succeeded, otherwise <see langword="false"/>.</returns>
		public static bool ChangeOwner( FileSystemInfo fsInfo, int owner, bool followTree = false ) => ChangeOwner( fsInfo.FullName, owner, followTree );

		/// <summary>
		/// Changes the group of the file or directory.
		/// </summary>
		/// <seealso cref="ChangeOwner(string, int, bool)"/>
		/// <seealso cref="ChangeOwner(Uri, int, bool)"/>
		/// <seealso cref="ChangeOwner(FileSystemInfo, int, bool)"/>
		/// <seealso cref="ChangeGroup(Uri, int, bool)"/>
		/// <seealso cref="ChangeGroup(FileSystemInfo, int, bool)"/>
		/// <seealso cref="ChangeOwnerAndGroup(string, int, int, bool)"/>
		/// <seealso cref="ChangeOwnerAndGroup(Uri, int, int, bool)"/>
		/// <seealso cref="ChangeOwnerAndGroup(FileSystemInfo, int, int, bool)"/>
		/// <param name="path">The path to the target.</param>
		/// <param name="group">The numerical value of the group.</param>
		/// <param name="followTree">Rather to go deeper into the directory or not.</param>
		/// <returns><see langword="true"/> if all succeeded, otherwise <see langword="false"/>.</returns>
		public static bool ChangeGroup( string path, int group, bool followTree = false ) => ChangeOwnerAndGroup( path, DONT_CHANGE, group, followTree );

		/// <summary>
		/// Changes the group of the file or directory.
		/// </summary>
		/// <seealso cref="ChangeOwner(string, int, bool)"/>
		/// <seealso cref="ChangeOwner(Uri, int, bool)"/>
		/// <seealso cref="ChangeOwner(FileSystemInfo, int, bool)"/>
		/// <seealso cref="ChangeGroup(string, int, bool)"/>
		/// <seealso cref="ChangeGroup(FileSystemInfo, int, bool)"/>
		/// <seealso cref="ChangeOwnerAndGroup(string, int, int, bool)"/>
		/// <seealso cref="ChangeOwnerAndGroup(Uri, int, int, bool)"/>
		/// <seealso cref="ChangeOwnerAndGroup(FileSystemInfo, int, int, bool)"/>
		/// <param name="path">The path to the target.</param>
		/// <param name="group">The numerical value of the group.</param>
		/// <param name="followTree">Rather to go deeper into the directory or not.</param>
		/// <returns><see langword="true"/> if all succeeded, otherwise <see langword="false"/>.</returns>
		public static bool ChangeGroup( Uri path, int group, bool followTree = false ) => ChangeGroup( path.AbsolutePath, group, followTree );

		/// <summary>
		/// Changes the group of the file or directory.
		/// </summary>
		/// <seealso cref="ChangeOwner(string, int, bool)"/>
		/// <seealso cref="ChangeOwner(Uri, int, bool)"/>
		/// <seealso cref="ChangeOwner(FileSystemInfo, int, bool)"/>
		/// <seealso cref="ChangeGroup(string, int, bool)"/>
		/// <seealso cref="ChangeGroup(Uri, int, bool)"/>
		/// <seealso cref="ChangeOwnerAndGroup(string, int, int, bool)"/>
		/// <seealso cref="ChangeOwnerAndGroup(Uri, int, int, bool)"/>
		/// <seealso cref="ChangeOwnerAndGroup(FileSystemInfo, int, int, bool)"/>
		/// <param name="fsInfo">The <see cref="FileSystemInfo"/> object representing the file or folder.</param>
		/// <param name="group">The numerical value of the group.</param>
		/// <param name="followTree">Rather to go deeper into the directory or not.</param>
		/// <returns><see langword="true"/> if all succeeded, otherwise <see langword="false"/>.</returns>
		public static bool ChangeGroup( FileSystemInfo fsInfo, int group, bool followTree = false ) => ChangeGroup( fsInfo.FullName, group, followTree );

		/// <summary>
		/// Changes the owner and group of the file or directory.
		/// </summary>
		/// <remarks>
		///	If the intended change is for only 1 (ie: either group or owner), then pass <see cref="DONT_CHANGE"/> constant into the one that is not meant to change.
		/// </remarks>
		/// <seealso cref="ChangeOwner(string, int, bool)"/>
		/// <seealso cref="ChangeOwner(Uri, int, bool)"/>
		/// <seealso cref="ChangeOwner(FileSystemInfo, int, bool)"/>
		/// <seealso cref="ChangeGroup(string, int, bool)"/>
		/// <seealso cref="ChangeGroup(Uri, int, bool)"/>
		/// <seealso cref="ChangeGroup(FileSystemInfo, int, bool)"/>
		/// <seealso cref="ChangeOwnerAndGroup(Uri, int, int, bool)"/>
		/// <seealso cref="ChangeOwnerAndGroup(FileSystemInfo, int, int, bool)"/>
		/// <param name="path">The path to the target.</param>
		/// <param name="owner">The numerical value of the owner.</param>
		/// <param name="group">The numerical value of the group.</param>
		/// <param name="followTree">Rather to go deeper into the directory or not.</param>
		/// <returns><see langword="true"/> if all succeeded, otherwise <see langword="false"/>.</returns>
		public static bool ChangeOwnerAndGroup( string path, int owner, int group, bool followTree = false ) => ChangeOwnerAndGroup(
			File.GetAttributes( path ).HasFlag( FileAttributes.Directory ) ? (FileSystemInfo) new DirectoryInfo( path ) : new FileInfo( path ),
			owner, group, followTree
		);

		/// <summary>
		/// Changes the owner and group of the file or directory.
		/// </summary>
		/// <remarks>
		///	If the intended change is for only 1 (ie: either group or owner), then pass <see cref="DONT_CHANGE"/> constant into the one that is not meant to change.
		/// </remarks>
		/// <seealso cref="ChangeOwner(string, int, bool)"/>
		/// <seealso cref="ChangeOwner(Uri, int, bool)"/>
		/// <seealso cref="ChangeOwner(FileSystemInfo, int, bool)"/>
		/// <seealso cref="ChangeGroup(string, int, bool)"/>
		/// <seealso cref="ChangeGroup(Uri, int, bool)"/>
		/// <seealso cref="ChangeGroup(FileSystemInfo, int, bool)"/>
		/// <seealso cref="ChangeOwnerAndGroup(string, int, int, bool)"/>
		/// <seealso cref="ChangeOwnerAndGroup(FileSystemInfo, int, int, bool)"/>
		/// <param name="path">The path to the target.</param>
		/// <param name="owner">The numerical value of the owner.</param>
		/// <param name="group">The numerical value of the group.</param>
		/// <param name="followTree">Rather to go deeper into the directory or not.</param>
		/// <returns><see langword="true"/> if all succeeded, otherwise <see langword="false"/>.</returns>
		public static bool ChangeOwnerAndGroup( Uri path, int owner, int group, bool followTree = false ) => ChangeOwnerAndGroup( path.AbsolutePath, owner, group, followTree );

		/// <summary>
		/// Changes the owner and group of the file or directory.
		/// </summary>
		/// <remarks>
		///	If the intended change is for only 1 (ie: either group or owner), then pass <see cref="DONT_CHANGE"/> constant into the one that is not meant to change.
		/// </remarks>
		/// <seealso cref="ChangeOwner(string, int, bool)"/>
		/// <seealso cref="ChangeOwner(Uri, int, bool)"/>
		/// <seealso cref="ChangeOwner(FileSystemInfo, int, bool)"/>
		/// <seealso cref="ChangeGroup(string, int, bool)"/>
		/// <seealso cref="ChangeGroup(Uri, int, bool)"/>
		/// <seealso cref="ChangeGroup(FileSystemInfo, int, bool)"/>
		/// <seealso cref="ChangeOwnerAndGroup(string, int, int, bool)"/>
		/// <seealso cref="ChangeOwnerAndGroup(Uri, int, int, bool)"/>
		/// <param name="fsInfo">The <see cref="FileSystemInfo"/> object representing the file or folder.</param>
		/// <param name="owner">The numerical value of the owner.</param>
		/// <param name="group">The numerical value of the group.</param>
		/// <param name="followTree">Rather to go deeper into the directory or not.</param>
		/// <returns><see langword="true"/> if all succeeded, otherwise <see langword="false"/>.</returns>
		public static bool ChangeOwnerAndGroup( FileSystemInfo fsInfo, int owner, int group, bool followTree = false ) {
			if ( !followTree || fsInfo is FileInfo ) {
				return ParseCallChown( fsInfo.FullName, owner, group );
			}

			var allSucceeded = true;

			foreach ( var fsChildInfo in ( (DirectoryInfo) fsInfo ).EnumerateFileSystemInfos( "*", SearchOption.AllDirectories ) ) {
				if ( ParseCallChown( fsChildInfo.FullName, owner, group ) ) {
					LogManager.Log( $"Successfully changed owner/group on `{fsChildInfo.FullName}`." );
				} else {
					allSucceeded = false;

					LogManager.LogError( $"Failed to change owner/group on `{fsChildInfo.FullName}`." );
				}
			}

			return allSucceeded;

			/* This local method is meant to wrap around the actual LibC method, which returns either a 0 or a 1.
			 * The 1 or 0 is useful for C/C++ (or any other language that can use 0 or 1 as false or true, but C# cannot. */
			static bool ParseCallChown( string path, int owner, int group ) => Convert.ToBoolean( chown( path, (uint) owner, (uint) group ) );
		}

		/// <summary>
		/// Changes the file modifiers.
		/// </summary>
		/// <param name="path">Path to target.</param>
		/// <param name="mod">The flags for the file.</param>
		/// <param name="followTree">Follow the directories down and apply it to all the files and folders.</param>
		/// <returns><see langword="true"/> if all succeeded, otherwise <see langword="false"/>.</returns>
		public static bool ChangeModifier( string path, Modifiers mod, bool followTree = false ) => ChangeModifier(
			File.GetAttributes( path ).HasFlag( FileAttributes.Directory ) ? (FileSystemInfo) new DirectoryInfo( path ) : new FileInfo( path ),
			mod, followTree
		);

		/// <summary>
		/// Changes the file modifiers.
		/// </summary>
		/// <param name="path">Path to target.</param>
		/// <param name="mod">The flags for the file.</param>
		/// <param name="followTree">Follow the directories down and apply it to all the files and folders.</param>
		/// <returns><see langword="true"/> if all succeeded, otherwise <see langword="false"/>.</returns>
		public static bool ChangeModifier( Uri path, Modifiers mod, bool followTree = false ) => ChangeModifier( path.AbsolutePath, mod, followTree );

		/// <summary>
		/// Changes the file modifiers.
		/// </summary>
		/// <param name="fsInfo">The <see cref="FileSystemInfo"/> of the target.</param>
		/// <param name="mod">The flags for the file.</param>
		/// <param name="followTree">Follow the directories down and apply it to all the files and folders.</param>
		/// <returns><see langword="true"/> if all succeeded, otherwise <see langword="false"/>.</returns>
		public static bool ChangeModifier( FileSystemInfo fsInfo, Modifiers mod, bool followTree = false ) {
			if ( !followTree || fsInfo is FileInfo ) {
				return ParseCallChmod( fsInfo.FullName, mod );
			}

			var allSucceeded = true;

			foreach ( var fsChildInfo in ( (DirectoryInfo) fsInfo ).EnumerateFileSystemInfos( "*", SearchOption.AllDirectories ) ) {
				if ( ParseCallChmod( fsChildInfo.FullName, mod ) ) {
					LogManager.Log( $"Successfully changed the modifier on `{fsChildInfo.FullName}`." );
				} else {
					allSucceeded = false;

					LogManager.LogError( $"Failed to change modifier on `{fsChildInfo.FullName}`." );
				}
			}

			return allSucceeded;

			/* This method wraps the call to chmod in LibC to output either a true or false call.
			 * Since 1 or 0 can be used in low level languages as true or false, C# does not operate this way, and as to be converted. */
			static bool ParseCallChmod( string path, Modifiers mod ) => Convert.ToBoolean( chmod( path, (uint) mod ) );
		}
	}
}