﻿using System;
using System.Runtime.InteropServices;

namespace FluffyBot.WinAPI {
	public static class Kernel32 {
		private enum ConsoleControlEvent : uint {
			CTRL_C_EVENT        = 0,
			CTRL_CLOSE_EVENT    = 2,
			CTRL_SHUTDOWN_EVENT = 6
		}

		private const string LIBRARY = "kernel32.dll";

		private static event Action<string> Handler;

		static Kernel32() {
			// To ensure that the Console class has been initialized.
			Console.CancelKeyPress += ( _, args ) => {};

			if ( !SetConsoleCtrlHandler( ConsoleCtrlHandler, true ) ) {
				throw Marshal.GetExceptionForHR( Marshal.GetHRForLastWin32Error() )!;
			}
		}

		public static void AddHandler( Action<string> handler ) => Handler += handler;

		private static bool ConsoleCtrlHandler( ConsoleControlEvent controlType ) {
			switch ( controlType ) {
				case ConsoleControlEvent.CTRL_CLOSE_EVENT:
				case ConsoleControlEvent.CTRL_C_EVENT:
				case ConsoleControlEvent.CTRL_SHUTDOWN_EVENT:
					Handler?.Invoke( Enum.GetName( controlType.GetType(), controlType ) );

					return true;
				default: return false;
			}
		}

		[UnmanagedFunctionPointer( CallingConvention.Winapi )]
		[return: MarshalAs( UnmanagedType.Bool )]
		private delegate bool SetConsoleCtrlHandler_HandlerRoutine( ConsoleControlEvent controlType );

		[DllImport( LIBRARY, SetLastError = true, CallingConvention = CallingConvention.Winapi )]
		[return: MarshalAs( UnmanagedType.Bool )]
		private static extern bool SetConsoleCtrlHandler( SetConsoleCtrlHandler_HandlerRoutine handler, [MarshalAs( UnmanagedType.Bool )] bool add );
	}
}