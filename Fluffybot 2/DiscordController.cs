﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using FluffyBot.Database;
using FluffyBot.Modules;
using FluffyBot.Utilities;
using Microsoft.Extensions.DependencyInjection;

namespace FluffyBot {
	public static class DiscordController {
		private static readonly CommandService          comService;
		private static readonly IServiceProvider        serviceProvider;
		private static readonly DiscordSocketClient     client;
		private static readonly LinkedList<LinkHandler> linkHandlers;

		static DiscordController() {
			var linkHandlerType = typeof( LinkHandler );

			client = new DiscordSocketClient(
				new DiscordSocketConfig() {
					DefaultRetryMode = RetryMode.AlwaysRetry,
					LogLevel         = LogSeverity.Verbose
				}
			);

			comService = new CommandService(
				new CommandServiceConfig() {
					CaseSensitiveCommands = true,
					SeparatorChar         = ';',
					LogLevel              = LogSeverity.Verbose,
					DefaultRunMode        = RunMode.Async
				}
			);

			serviceProvider = new ServiceCollection()
							  .AddSingleton( client )
							  .AddSingleton( comService )
							  .AddScoped<FluffyBotContext>()
							  .BuildServiceProvider();

			_ = comService.AddModulesAsync( typeof( DiscordController ).Assembly, serviceProvider )
						  .GetAwaiter()
						  .GetResult();

			client.Log             += Logging;
			client.MessageReceived += MessageReceived;

			linkHandlers = new LinkedList<LinkHandler>();

			foreach ( var asm in AppDomain.CurrentDomain.GetAssemblies() ) {
				foreach ( var type in asm.GetTypes() ) {
					if ( type.IsAbstract || type.IsInterface || type.IsEnum || !linkHandlerType.IsAssignableFrom( type ) ) {
						continue;
					}

					var handler = (LinkHandler) Activator.CreateInstance( type, new object[0] );

					linkHandlers.AddLast( handler );
				}
			}
		}

		internal static Task InitializeBotConnection() {
			// There to help with different pre-processor actions.
			// ReSharper disable once JoinDeclarationAndInitializer
			string token;
			
			token = SettingsManager.GetSetting<string>( "Token" );

			client.LoginAsync( TokenType.Bot, token ).ConfigureAwait( false ).GetAwaiter().GetResult();

			return client.StartAsync();
		}

		private static async Task MessageReceived( SocketMessage msg ) {
			var argPos = 0;

			if ( !( msg is SocketUserMessage message ) || message.Author.IsBot ) {
				return;
			}

			foreach ( var handler in linkHandlers.Where( handler => handler.ContainsValidLink( msg.Content ) ) ) {
				handler.ProcessLinkAsync( msg.Content, msg.Author, msg.Channel );
			}

			if ( message.HasStringPrefix( "f!", ref argPos ) ) {
				var context = new SocketCommandContext( client, message );
				var result  = await comService.ExecuteAsync( context, argPos, serviceProvider );

				if ( !result.IsSuccess ) {
					// ReSharper disable once SwitchStatementHandlesSomeKnownEnumValuesWithDefault
					switch ( result.Error ) {
						case CommandError.BadArgCount:
							await message.Channel.SendMessageAsync( $"{message.Author.Mention}, you have provided the wrong of arguments for the command." );
							break;
						case CommandError.Exception:
							LogManager.LogError( $"Problem occured mid execution of command.\n{result.ErrorReason}" );
							await message.Channel.SendMessageAsync( $"{message.Author.Mention}, sorry, unable to satisfy your request.\nThe following problem occured:\t{result.ErrorReason}" );
							break;
						case CommandError.MultipleMatches:
							LogManager.LogError( result.ErrorReason );
							await message.Channel.SendMessageAsync( $"{message.Author.Mention}, sorry, I don't know which command to do as I know of multiple that are a match." );
							break;
						case CommandError.ParseFailed:
							await message.Channel.SendMessageAsync( $"{message.Author.Mention}, I have failed to understand your input." );
							break;
						case CommandError.UnknownCommand:
							await message.Channel.SendMessageAsync( $"{message.Author.Mention}, I do not understand that command." );
							break;
						case CommandError.UnmetPrecondition:
							await message.Channel.SendMessageAsync( $"{message.Author.Mention}, I am sorry, but a precondition has failed. Please contact the bot owner about the command." );
							break;
						case CommandError.Unsuccessful:
						case CommandError.ObjectNotFound:
							await message.Channel.SendMessageAsync( $"{message.Author.Mention}\n__**SOMETHING HAPPENED**__\n Do not know what, but something happened." );
							break;
					}
				}
			}
		}

		private static Task Logging( LogMessage arg ) {
			switch ( arg.Severity ) {
				case LogSeverity.Info:
				case LogSeverity.Verbose:
				case LogSeverity.Debug:
					LogManager.Log( arg.Message );
					break;
				case LogSeverity.Warning:
					LogManager.LogWarning( arg.Message );
					break;
				case LogSeverity.Error:
					LogManager.LogError( arg.Message );
					break;
				case LogSeverity.Critical:
					LogManager.LogException( arg.Message, arg.Exception );
					break;
			}

			return Task.CompletedTask;
		}
	}
}