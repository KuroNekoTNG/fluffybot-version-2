﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FluffyBot.Attributes {
	[AttributeUsage( AttributeTargets.Class, Inherited = false, AllowMultiple = false )]
	public sealed class RunStaticConstructorAttribute : Attribute {}
}