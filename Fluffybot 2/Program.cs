﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using FluffyBot.Attributes;
using FluffyBot.Nix;
using FluffyBot.Utilities;

namespace FluffyBot {
	internal static class Program {
		private const int NIX_ID = 666;

		public static Task Main( string[] args ) {
			#if !DEBUG
			Initialize();
			#else
			CommonInit();
			#endif

			DiscordController.InitializeBotConnection().ConfigureAwait( false ).GetAwaiter().GetResult();

			return Task.Delay( -1 );
		}

		private static void Initialize() {
			if ( Environment.OSVersion.Platform == PlatformID.Unix ) {
				UnixInit();

				return;
			}

			GeneralInit();
		}

		// For initialization on Unix/Linux/Unix-Like.
		private static void UnixInit() {
			const string ROOT_MSG        = "Please start the bot as root.";
			var          containerEnvVar = Environment.GetEnvironmentVariable( "DOTNET_RUNNING_IN_CONTAINER" );

			// Checks to see if it is root or not. If it is not, it throws an exception.
			if ( LibC.UserID != 0 ) {
				throw new UnauthorizedAccessException( ROOT_MSG );
			}

			CommonInit();

			if ( string.IsNullOrWhiteSpace( containerEnvVar ) || ( !string.IsNullOrWhiteSpace( containerEnvVar ) && bool.TryParse( containerEnvVar, out var val ) && !val ) ) {
				// Ensures that the Owner and Group are FluffyBot's.
				LibC.ChangeOwnerAndGroup( SettingsManager.SETTING_FOLDER, NIX_ID, NIX_ID, true );
				LibC.ChangeOwnerAndGroup( LogManager.MAIN_LOG_FOLDER, NIX_ID, NIX_ID, true );

				// Drop's down to a safer user and group.
				LibC.GroupID = NIX_ID;
				LibC.UserID  = NIX_ID;
			}

			LogManager.Log( "Everything seems to be good." );
		}

		private static void GeneralInit() {
			CommonInit();

			LogManager.Log( "Everything seems good." );
		}

		private static void CommonInit() {
			CheckInternetConnectivity();
			RunStaticConstructors();

			/* Methods that only Common Init will call, no other init will call this.
			 * These methods are here to help better organize scopes. */
			#region Local Methods
			static void CheckInternetConnectivity() {
				var wait = 5000; // Wait is in milliseconds.
				
				while ( !NetworkInterface.GetIsNetworkAvailable() ) {
					LogManager.LogError( $"Unable to connect to the internet. Make sure that you have configured everything correctly.\nNext check is in `{wait/1000}` seconds." );
					
					Task.Delay( wait ).GetAwaiter().GetResult();

					if ( wait + 5000 < 300000 ) {
						wait += 5000;
					}
				}
			}

			static void RunStaticConstructors() {
				var myAsm = typeof( Program ).Assembly;
				var types = myAsm.GetTypes();

				foreach ( var type in types ) {
					if ( type.GetCustomAttributes( typeof( RunStaticConstructorAttribute ), false ).Length == 1 ) {
						RuntimeHelpers.RunClassConstructor( type.TypeHandle );
					}
				}
			}
			#endregion
		}
	}
}