﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Runtime.InteropServices;
using FluffyBot.Attributes;
using FluffyBot.Utilities;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace FluffyBot.Database {
	[RunStaticConstructor]
	[SuppressMessage( "ReSharper", "UnusedAutoPropertyAccessor.Global" )]
	public sealed class FluffyBotContext : DbContext {
		public static string DatabaseFile { get; }

		private static bool exists;

		static FluffyBotContext() {
			DatabaseFile = Path.Combine( SettingsManager.SETTING_FOLDER, "db.sqlite" );

			if ( !File.Exists( DatabaseFile ) ) {
				File.Create( DatabaseFile ).Dispose();
			}
		}

		public DbSet<Image> Images { get; set; }

		public FluffyBotContext() {
			if ( exists ) {
				return;
			}

			if ( File.Exists( DatabaseFile ) ) {
				return;
			}

			if ( !Directory.Exists( Path.GetDirectoryName( DatabaseFile ) ) ) {
				_ = Directory.CreateDirectory( Path.GetDirectoryName( DatabaseFile ) );
			}

			Database.EnsureCreated();
			exists = true;
		}

		public FluffyBotContext( DbContextOptions<FluffyBotContext> options ) : base( options ) {
		}

		protected override void OnConfiguring( DbContextOptionsBuilder optionsBuilder ) {
			if ( optionsBuilder.IsConfigured ) {
				return;
			}

			var builder = new SqliteConnectionStringBuilder() {
				Cache      = SqliteCacheMode.Private,
				Mode       = SqliteOpenMode.ReadWriteCreate,
				DataSource = DatabaseFile
			};
			var opts = builder.ToString();

			optionsBuilder.UseSqlite( opts );
		}
	}
}