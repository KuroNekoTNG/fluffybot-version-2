﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace FluffyBot.Database {
	[SuppressMessage( "ReSharper", "UnusedAutoPropertyAccessor.Global" )]
	public class Image {
		[Required]
		public ulong UserID {
			get;
			set;
		}

		[Required]
		public string Url {
			get;
			set;
		}

		[Required]
		public string Name {
			get;
			set;
		}

		[Required]
		public Guid Id {
			get;
			set;
		}
	}
}